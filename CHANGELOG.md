# Changelog

**Tipos de cambios**
- Añadido `Added` para funcionalidades nuevas.
- Modificado `Changed` para los cambios en funcionalidades existentes.
- Obsoleto `Deprecated` para indicar que una funcionalidad está obsoleta, se queda sin soporte y se eliminará en próximas versiones.
- Eliminado `Removed` para las funcionalidades en desuso que se eliminaron en esta versión.
- Corregido `Fixed` para corrección de errores.
- Seguridad `Security` en caso de vulnerabilidades.
<br>

---
## Comadreja [**1.1.3**] <sub>2022-02-01</sub>

### Added
    Funcionalidades para login custom (temporalmente, en maduración AAD para gestión de usuarios externos)
### Fixed
    Mensaje de error en la platilla cuando no se logra conexión con Authentication (Nuevo SSO)

## Comadreja [**1.1.2**] <sub>2022-12-19</sub>

### Fixed
    SecureStorageService sitio no carga cuando se tienen datos almacenados previamente en storage

## Comadreja [**1.1.1**] <sub>2022-12-14</sub>

### Added
    Nuevo FilterByPropModule 'shared-modules/pipes/filter-by-prop/*'
    Nuevo ExampleFilter 'shared-modules/pipes/filter-by-prop/*'

### Fixed
    Cambio de ubicación SafeResourcePipe a shared-modules

## Comadreja [**1.1.0**] <sub>2022-12-14</sub>

### Added
    Environment templates por métodos de autenticación
    .dockerignore para archivo dockerfile (https://sites.google.com/coppel.com/developers/estandares/contenedores?authuser=0#h.teppf7oot79k)

### Removed
    Funcionalidades para login custom para fin de apego a norma de Santiago Ontañon para anulación de diversidad de logins
    Variable deviceId (SSO) en archivos de environment

## Comadreja [**1.0.2**] <sub>2022-12-07</sub>

### Fixed
    Dependencia minimist declarada en fake-api/package.json
    Colocación de data titles para routing lazy loading de módulos

### Changed
    Environment cambio de SSO variable appName por channel obtenido del DNS para recaptcha

## Comadreja [**1.0.1**] <sub>2022-11-29</sub>

### Changed
    Cambios de estilos en verificación de componente huella (https://sites.google.com/coppel.com/developers/frameworks/webclient/componentes/componente-huella?authuser=0)

## Comadreja [**1.0.0**] <sub>2022-11-29</sub>

### Changed
    Cambio de versión de beta a release candidate
    Cambio de plataforma de testing Jasmine por Jest

## Comadreja [**0.0.0**] <sub>2022-11-28</sub>

### Added
    Se añade el archivo README.md en donde se describe el contenido del proyecto
    Se añade el arcvhido CHANGELOG.md con un registro de cambios a lo largo del tiempo