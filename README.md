# @coppelframework/webclient-angularspa

Este projecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version `14.2.10`

**@coppelframework/webclient v3.0.0** Es un proyecto de plantilla que tiene como base `AngularV14` que provee un punto de inicio para aplicaciones WEB. El cual cuanta con las mejores practicas de designacion, en el arbol de directorios, basados en las reglas de `Angular` esto permite identificar facilmente donde se encuentra colocados los distintos componentes que van a conformar la aplicacion a desarrollar.

---

## Prerrequisitos

Se necesitan los siguientes prerrequisitos para poder realizar la instalación:

- `NodeJS v14.x `o superior.
- `Git`.

---

## Dependencias

Se necesita tener instaladas las siguientes dependencias de manera **global**:

- `npm install -g @angular/cli`

---

## Lista de plugins VSCode

Se recomienda tener instalados los siguientes **plugins** para facilitar el desarrollo de las futuras aplicaciones:

- `Angular Essentials` (`Version 12`)
- `Bootstrap 4 & Font awesome snippets`
- `ESLint`
- `GitLens`
- `HTML Snippets`
- `Bracket Pair Colorizer`
- `Prettier - Code formatter`
- `SonarLint`

Si desea conocer acerca de cada **plugin** y su funcionamiento leer su documentación oficial.

---

## Descargar Plantilla

Es necesario descargar la plantilla de la siguiente `url`: http://gitlab.coppel.io/oc/coppelframework-webclient-angularspa/-/archive/master/coppelframework-webclient-angularspa-master.zip

Se descargara un archivo `.zip`, se tiene que **descomprimir** en la ruta que sea más conveniente y renombrar la carpeta con el nombre correspondiente.

Una vez realizado lo anterior, hay que posicionarse dentro de la carpeta, y ejecutar el comando en el folder del proyecto y en el folder de fake-api:

```npm
npm install
```

Este comando es para instalar las **dependencias** necesarias para el entorno de desarrollo y de la aplicación, una vez que las **dependencias** están instaladas ejecutar el siguiente comando para ejecutar la aplicación de manera local.

```npm
npm run dev
```

Este comando crea una instancia `http server` en el puerto `4200`, esto para poder probar la aplicación base creada.

_El servidor que se ejecuta, es el equivalente a un `XAMPP`, no es productivo, solamente sirve para el entorno de desarrollo._

---

A continuacion se describe el **scaffolding**.

---

## Estructura de Directorios 4.x.x (Angular v14.x)

### src

Es el directorio que contiene a toda nuestra aplicacion, desde los modulos hasta variables de ambientes, los cuales se
describen a continuacion:

---

### app

Este directorio es el principal de la aplicacion en el se encuentra los modulos, directivas, servicios, modelos, etc.

---

### app/containers

Este directorio contiene dos componentes que sirven para renderizar la aplicacion, cada uno para un caso en especifico:
full-layout, es para mostra menu y navbar, mientras que simple-layout no los contiene.

**_<sub>Nota: Cualquier cambio en esta seccion conlleva a tener que voler a realizarlos en versiones nuevas<sub>_**

---

## app/directives

En esta carpeta se encuentran las directivas de uso general, asi como pipes para toda la aplicacion.

---

## app/guards

Este directorio contiene un middleware, que pueden consumir las rutas definidas en nuestra app, para su acceso.

---

## app/helpers

Este directorio contiene un interceptor para incrustar el jwt en cada peticiòn que se haga a la api correspondiente.

**_<sub>Nota: Cualquier cambio en esta seccion conlleva a tener que voler a realizarlos en versiones nuevas<sub>_**

---

## app/models

Este directorio contiene interfaces y/o clases genericas que se pueden implementar en los componentes.

---

### app/modules

En este directorio se encuentran los modulos que se iran creando conforme el sistema o app crezca.
En el existen dos archivos principales que deben generarse:

- [nombremodulo].module.ts:\*\* Contiene la configuracion del modulo.
- [nombremodulo]-routing.module.ts: Contiene las rutas y subrutas para el modulo correspondiente.
- [componentes]: Adicionalmente cada moduloc cuenta con (n) cantidades de componentes que va a contener dicho modulo.

Asi mismo, este directorio puede contener si asi se requiere
los directorios:

- models: interfaces/clases - definiciones.
- services: servicios propios de dicho modulo
- modals: componentes modales
- directives: directivas propias
- styles: estilos propios (scss).

---

### app/services

Contiene servicios de uso generico de la aplicación, como lo son la autenticacion, y menú.

---

### app/services/menu.service.ts

Servicio que contiene ejemplo para consumir menu.json para construir el menu, en dado caso sustituir la url por la api de su aplicacion que genere el menu.

---

### app/shared-modules

Este directorio contiene componentes propios de coppel que son de uso compartido entre la aplicacion entre ellos. En esta carpeta se pueden ir incorporando esos componentes exlcusivos para la aplicacion.

---

### assets

En este directorio se encuentra archivos referentes a imagenes, pdf, o cualquier otro archivo requerido por la aplicacion (contenido estatico).

---

### environments

En este directorio se encuentran archivos referentes a los **ambientes** de aplicacion:

**environment.ts**: Contiene las variables de configuracion de un entorno de desarrollo o pruebas.
**environment.prod.ts**: Contiene las variables de configuracion de un entorno productivo.

```
    {
        appId: '',
        production: true,
        configFile: 'assets/settings/settings.prod.json',
        recaptcha: {
            scriptUrl: 'https://www.google.com/recaptcha/enterprise.js',
            siteKey: ''
        },
        authMechanisms: {
            azureAD: true,
            huella: true,
            colaboradorDigital: true,
            authCode: true
        },
        authIdp: {
            AAD: {
                tenantId: '',
                clientId: '',
                authorityUrl: 'https://login.microsoftonline.com',
                redirectUri: 'http://localhost:4200',
                graphUrl: 'https://graph.microsoft.com',
            },
            SSO: {
                appId: '',
                appKey: '',
                appName: 'CoppelFramework Webclient',
                deviceId: 'default',
                apiUrl: 'https://authentication.coppel.io/api/sso'
            }
        },
        interceptorApproach: [
            'http://localhost:3200'
        ],
        idpSpecificationRequired: true
    }
```

Los valores de las variables en la estructura del archivo corresponden a las configuraciones siguientes:

* appId(string):
	Código individual la de la aplicación.
    <br>&ensp;a) AAD: `appId`
    <br>&ensp;b) SSO: `clientId`
* production(boolean):
	Tipo de ambiente productivo(true); por default es false.
* configFile(string):
	Ruta donde se obtendrán los datos configuracion para la aplicación
    <br>&ensp;a) Archivo estático: Deberá contener la ruta relativa del archivo a partir de src (assets/settings/settings.json).
    <br>&ensp;b) API de gestión: Deberá contener la ruta completa del api que lo gestiona incluyendo http/https y puertos.
* recaptcha(string):
	Credenciales fijas para recaptcha
    <br>&ensp;- scriptUrl: URL fija para script de google recaptcha.
    <br>&ensp;- siteKey: Clave fija de captcha para desarrollo o productivo.
* authMechanisms({[mechanism]:boolean}):
	Indica los tipos de mecanismos de autenticación que se habilitarán para la aplicación.
    <br>&ensp;- azureAD: Azure Active Directory (AAD)
    <br>&ensp;- huella: Huella Digital (SSO)
    <br>&ensp;- colaboradorDigital: Correo y contraseña de Colabor Digital (SSO)
    <br>&ensp;- authCode: Token de aplicación (SSO)
* authIdp({[idp]:dict}):
    Claves para el IdP que crea, almacena y administra identidades digitales que estarán en uso en la aplicación
    <br>&ensp;a) AAD: Requiere solicitar un acceso único de aplicación Microsoft. Este proceso podrá solicitarse a operaciones TI.
    <br>&ensp;b) SSO: Requiere solicitar un acceso único de aplicación Coppel Authentication. Este proceso podrá solicitarse a operaciones TI.
* interceptorApproach:
	APIs y fuentes de datos que requieran recibir el token y headers del interceptor
* idpSpecificationRequired:
	Define si el interceptor deberá añadir a las peticiones el IdP que validará las claves de usuario

**_<sub>Nota: El uso del mecanismo ADD es experimental. Puede encontrar la documentación referente a AAD en el siguiente enlace:<sub>_**
	- https://docs.google.com/document/d/1fV5iTY0PoHLrDWTi85Jijus6H-ilt7vRpEf1_A-vid4

---

### settings

En este directorio se encuentran archivos referentes a las **variables** de aplicación:

**settings.ts**: Contiene las variables de configuracion de la aplicación de desarrollo o pruebas.
**settings.prod.ts**: Contiene las variables de configuracion de la aplicación productiva.

A estos archivos pueden pueden ir agregando atributos para usos exclusivos de la aplicación. Para accesar a dicho archivo se utiliza el servicio `ConfigService` el cual expone el metodo `getConfig()`; este retornara el objecto definido en dicho archivo.

- localSSO(string): Indica la url de la api del fake server para obtencion de credenciales y autenticacion.
- haveMenu(boolean): Indica si la aplicacion contara con algun menu lateral.
- appLogo(string): Indica la ruta del archivo a mostrar como logotipo de la aplicación.
- footerTitle: Indica el titulo para el pie de la aplicacion ej. ('sistema de bodegas').
- SSO(string): Indica la url de la api para utilizacion de SSO.

---

### img

Este directorio contiene contenido estatico de imagenes.

---

### scss

Este directorio contiene los estilos que se implementan en la aplicacion.
**_<sub>Nota: Este folder esta restringida su modificación<sub>_**

---

## Servidor de Desarrollo - ApiRest Desarrollo

Ejecuta en la terminal `npm run dev` para instanciar localmente un servidor web. y a su vez un servidor api-rest de desarrollo, `http://localhost:3200`, para el front, navega a la ruta `http://localhost:4200/`. La aplicación se recargara automaticamente si modificas cualquier archivo del codigo fuente.

---

## Servidor de Desarrollo

Ejecuta en la terminal `npm run start` para instanciar localmente un servidor web, navega a la ruta `http://localhost:4200/`. La aplicación se recargara automaticamente si modificas cualquier archivo del codigo fuente. **Nota** Deberan estar ya creados
los endpoints correspondientes (menu, login), que le corresponden a este frontend, de lo contrario marcara errores.

---

## Servidor de Desarrollo DNS - Colaborador Digital, ApiRest Desarrollo

Ejecuta en la terminal `npm run dns-json` para instanciar localmente un servidor web con un custom host o dns y a su vez un servidor api-rest de desarrollo, `http://localhost:3200`, para el front, navega a la ruta `http://localhost:4200/`. La aplicación se recargara automaticamente si modificas cualquier archivo del codigo fuente.

---

## Servidor de Desarrollo - Colaborador Digital

Ejecuta en la terminal `npm run dns` para instanciar localmente un servidor web con un custom host o dns, navega a la ruta `http://localhost:4200/`. La aplicación se recargara automaticamente si modificas cualquier archivo del codigo fuente. **Nota** Deberan estar ya creados
los endpoints correspondientes (menu, login), que le corresponden a este frontend, de lo contrario marcara errores.

---

## Lista basica de comandos para generar modulos y componentes

Ejecuta en la terminal `ng generate module module-name --routing` para generar un modulo nuevo, con su arcihvo de ruta.

Ejecuta en la terminal `ng generate component component-name` para generar un nuevo componente.

Adicionalmente existen estos diferentes comandos `ng generate directive/pipe/service/class`.

Todos los servicios globales de la aplicacion deben estar declarados en el modulo principal (app.module.ts), con sus imports correspondientes.

---

## Compilar (Build)

Ejecuta en la terminal `npm run build` para generar un distribuible de la aplicación. La distribucion sera almacenada en el directorio `dist/`.

---

## Running unit tests

Ejecuta en la terminal `ng test` para ejecutar pruebas unitarias via [Jest](https://jestjs.io/).

---

## Más Ayuda

Mas ayuda para los comandos de Angular CLI use `ng help` o ir a [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

---

## Obtener Datos de Sesion

Los datos de la persona logueada en el sistema se pueden accesar atravez del servicio
`SessionService`, el cual expone el observable `currentUser`.
Este devolvera un objecto con los datos del empleado, la estructura, puede variar dependiendo el tipo de sesion que entrega la sesión(authIdp).

---

## Archivo db.json

Este archivo contiene un mock de base de datos, de tipo json, el cual sirve para hacer pruebas en modo local simulando, las
distintas salidas o contrato de interfaces para las apis que se va a conectar el front-end.

---

## Pasos para Actualizar a esta version

Para actualizar a esta versión es necesario seguir los pasos que se indican en el Developers Coppel en la ruta siguiente:

- [Developers/frameworks/webclientV3/mantenimiento](https://sites.google.com/coppel.com/developers/frameworks/v3/mantenimiento?authuser=0#h.8f85qqwux70p)

