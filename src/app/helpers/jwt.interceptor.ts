import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SecureStorageService } from '../services/secure-storage.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private storageService: SecureStorageService) {}

  private setTokenHeaders(request: HttpRequest<any>) {
    let headers: Record<string, string> = { 
      'Authorization': this.storageService.getItem('token')
    }
    if (environment.idpSpecificationRequired) {
      headers['IdP'] = this.storageService.getItem('idp')
    }
    return request.clone({ setHeaders: headers });
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    let requireAuth = environment.interceptorApproach.some(source => request.url.includes(source));
    if (requireAuth) {
      if (localStorage.getItem('idp') && localStorage.getItem('token')) {
        request = this.setTokenHeaders(request);
      }
    }
    return next.handle(request)
  }

}