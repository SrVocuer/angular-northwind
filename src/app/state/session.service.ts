import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  
  private currentUserSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public readonly currentUser: Observable<any> = this.currentUserSubject.asObservable();

  constructor(
    private router: Router
  ) {
  }

  setCurrentUser(currentUser: any): void {
    this.currentUserSubject.next(currentUser);
  }

  public logout() {    
    localStorage.clear()
    sessionStorage.clear()
    this.router.navigate(['/login']);
    return false
  }

}
