import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

import localeEsMx from '@angular/common/locales/es-MX';
registerLocaleData(localeEsMx);

// Import Global Modules
import { AppRoutingModule } from './app-routing.module';
import { ToasterModule } from '@app-toaster';
import { LoaderModule } from '@app-loader';

// Import Global Services
import { AadService } from './services/idps/aad.service';
import { ConfigService } from './services/config.service';
import { SecureStorageService } from './services/secure-storage.service';
import { DevguideService } from './services/devguide.service';

// Import Global Helpers
import { AuthGuard } from './guards/Auth.guard';
import { JwtInterceptor } from './helpers/jwt.interceptor';

// Import containers
import { FullLayoutModule } from './containers/full-layout/full-layout.module';
import { SimpleLayoutModule } from './containers/simple-layout/simple-layout.module';

// Import 3rd party components
import { InteractionType, PublicClientApplication } from '@azure/msal-browser';
import {
  MsalGuard,
  MsalGuardConfiguration,
  MsalInterceptor,
  MsalInterceptorConfiguration,
  MsalModule,
  MsalRedirectComponent
} from '@azure/msal-angular';

import { CategoryComponent } from './modules/category/category.component';
import { EmployeeComponent } from './modules/employee/employee.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SuppliersComponent } from './modules/suppliers/suppliers.component';

import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { ProductsComponent } from './modules/products/products.component';
import { ClientesComponent } from './modules/clientes/clientes.component';
import { ProductsModule } from './modules/products/products.module';


// MSAL Config
const isIE = window.navigator.userAgent.indexOf('MSIE ') > -1 || window.navigator.userAgent.indexOf('Trident/') > -1;
const { tenantId, clientId, authorityUrl, redirectUri, graphUrl } = environment.authIdp.AAD;
const msalAuthConfiguration = {
  auth: {
    clientId,
    authority: `${authorityUrl}/${tenantId}`,
    redirectUri: redirectUri
  }, cache: { cacheLocation: 'sessionStorage', storeAuthStateInCookie: isIE }
};
const msalGuardConfiguration: MsalGuardConfiguration = {
  interactionType: InteractionType.Redirect, // MSAL Guard Configuration
  authRequest: { scopes: ['User.Read'] }
}
const msalInterceptorConfiguration: MsalInterceptorConfiguration = {
  interactionType: InteractionType.Redirect, // MSAL Interceptor Configuration
  protectedResourceMap: new Map([[`${graphUrl}/v1.0/me`, ['User.Read']]])
}

// App Initializer Config
export function ConfigLoader(configService: ConfigService) {
  return () => configService.getJSON();
}
export function StorageLoader(storageService: SecureStorageService) {
  return () => {
    storageService.load()
    return storageService.loaded
  };
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ProductsModule,
    ToasterModule,
    LoaderModule,
    SimpleLayoutModule,
    FullLayoutModule,
    NgxDatatableModule,
    ModalModule,
    MsalModule.forRoot(
      new PublicClientApplication(msalAuthConfiguration),
      msalGuardConfiguration,
      msalInterceptorConfiguration
    )
  ],
  declarations: [AppComponent, CategoryComponent, EmployeeComponent, SuppliersComponent, ProductsComponent, ClientesComponent],
  providers: [
    ConfigService,
    SecureStorageService,
    AadService,
    BsModalService,
    { provide: LOCALE_ID, useValue: 'es-MX' },
    { provide: APP_INITIALIZER, useFactory: ConfigLoader, deps: [ConfigService], multi: true },
    { provide: APP_INITIALIZER, useFactory: StorageLoader, deps: [SecureStorageService], multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: MsalInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AuthGuard,
    MsalGuard,
    DevguideService
  ],
  bootstrap: [AppComponent, MsalRedirectComponent],
})
export class AppModule { }
