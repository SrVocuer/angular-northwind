import { Component, OnInit } from '@angular/core';
import { ToasterService } from './services/toaster.service';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.scss']
})
export class ToasterComponent implements OnInit {

  show = false;
  type: 'error' | 'warn' | 'info' | 'message' | 'approval';
  body: string;
  title: string;

  constructor(private toasterService: ToasterService) {
    this.type = 'message';
    this.body = '';
    this.title = '';
  }

  ngOnInit(): void {
    this.listenMessages()
  }

  /**
    * listenMessages
    */
  private listenMessages() {
    this.toasterService.$message.subscribe({
      next: (payload) => {
        if (!this.show && payload) {
          this.type = payload.type || "message";
          this.body = payload.body || "";
          this.title = payload.title;
          this.show = true;
          setTimeout(() => { this.show = false }, 20000);
        }
      }
    })
  }

}