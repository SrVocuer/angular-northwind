import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class CaptchaApiService {

  constructor() {
    if (environment.authMechanisms.colaboradorDigital) {
      this.loadScript();
    }
  }

  /**
   * getToken
   */
  public getToken(action: string): Promise<string> {
    // @ts-ignore
    const grecaptcha = window.grecaptcha;
    return new Promise((resolve, reject) => {
      if (grecaptcha) {
        grecaptcha.enterprise.ready(() => {
          grecaptcha.enterprise.execute(environment.recaptcha.siteKey, { action: action }).then(
            (token: string) => resolve(token),
            (error: { message: any; }) => reject(error && error.message || 'Captcha request error.')
          )
        });
      } else {
        reject('reCAPTCHA v3 is not loaded correctly.');
      }
    })
  }
  /**
   * loadScript
   */
  public loadScript() {
    return new Promise((resolve, reject) => {
      const scriptWithParams = `${environment.recaptcha.scriptUrl}?render=${environment.recaptcha.siteKey}`
      const scriptIdx = Array.from(document.scripts).findIndex(script => script.src == scriptWithParams)
      let script: any = scriptIdx != -1
        ? document.scripts.item(scriptIdx)
        : document.createElement('script');
      if (scriptIdx == -1) {
        script.type = 'text/javascript';
        script.src = scriptWithParams;
      }
      this.awaitReadyScript(script).then(response => resolve(response))
      script.onerror = (error: any) => reject({ loaded: false, status: 'Loaded' });
      document.getElementsByTagName('head')[0].appendChild(script);
    })
  }
  /**
   * awaitReadyScript
   */
  private awaitReadyScript(script: { readyState: string; onreadystatechange: (() => void) | null; loaded: boolean; onload: () => void; }) {
    return new Promise((resolve) => {
      if (script.readyState) {  //IE
        script.onreadystatechange = () => {
          if (script.readyState === "loaded" || script.readyState === "complete") {
            script.onreadystatechange = null;
            script.loaded = true;
            resolve({ loaded: true, status: 'Loaded' });
          }
        };
      } else {  //Others
        script.onload = () => {
          script.loaded = true;
          resolve({ loaded: true, status: 'Loaded' });
        };
      }
    })
  }

}

