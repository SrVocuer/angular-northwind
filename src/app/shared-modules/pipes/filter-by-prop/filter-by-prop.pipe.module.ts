import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterByPropPipe, FilterBySubPropsPipe } from './filter-by-prop.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FilterByPropPipe,
    FilterBySubPropsPipe
  ],
  exports: [
    FilterByPropPipe,
    FilterBySubPropsPipe
  ]
})
export class FilterByPropModule { }
