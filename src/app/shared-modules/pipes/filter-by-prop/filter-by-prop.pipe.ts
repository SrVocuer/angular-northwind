import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByProp',
  pure: false
})
// https://coderjony.com/blogs/creating-a-pipe-to-filter-the-items-in-the-list-using-angular-7
export class FilterByPropPipe implements PipeTransform {

  transform(items: any[], fieldName: string, search: any): any[] {

    if (!items) { return []; }
    if (!search || !fieldName) { return items; }
    if (typeof search != "string") search = search.toString();

    return items.filter(item => {
      if (!item || !item[fieldName]) { return []; }
      let value = item[fieldName]
      if (typeof value != "string") value = value.toString();
      return value.toLowerCase().includes(search.toLowerCase());
    });
  }

}

@Pipe({
  name: 'filterBySubProps',
  pure: false
})
export class FilterBySubPropsPipe implements PipeTransform {

  private getFieldPath(item: any, fieldTree: string[]) {
    let prop = item;
    for (let i = 0; i < fieldTree.length; i++) {
      prop = prop[fieldTree[i]];
      if (!prop) { return undefined; }
    }
    return prop
  }

  transform(items: any[], fieldTree: string[], fields: string[], search: any): any[] {

    if (!items) { return []; }
    if (!search || !fieldTree) { return items; }
    if (!Array.isArray(fieldTree)) { return items; }

    if (typeof search != "string") { search = search.toString(); }

    return items.filter(item => {
      const objOnPath = this.getFieldPath(item, fieldTree);
      if (!objOnPath) { return false; }
      const concatenatedValues: any = fields.reduce((obj, key) => {
        let value = objOnPath[key];
        if (typeof value != "string") value = value.toString();
        return obj.concat(value)
      }, '');
      return concatenatedValues.toLowerCase().includes(search.toLowerCase());
    });

  }

}