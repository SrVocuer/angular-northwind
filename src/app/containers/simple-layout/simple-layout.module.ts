import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SimpleLayoutComponent } from './simple-layout.component';

@NgModule({
  declarations: [
    SimpleLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    SimpleLayoutComponent
  ]
})
export class SimpleLayoutModule { }
