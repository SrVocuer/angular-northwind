import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Import components
import { FullLayoutComponent } from './full-layout.component';
import {
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  AppSidebarNavComponent,
  AppSidebarNavDropdownComponent,
  AppSidebarNavItemComponent,
  AppSidebarNavLinkComponent,
  AppSidebarNavTitleComponent,
} from './components';
const LAYOUT_COMPONENTS = [
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  AppSidebarNavComponent,
  AppSidebarNavDropdownComponent,
  AppSidebarNavItemComponent,
  AppSidebarNavLinkComponent,
  AppSidebarNavTitleComponent,
];

// Import global directives
import {
  AsideToggleDirective,
  NavDropdownDirective,
  NavDropdownToggleDirective,
  ReplaceDirective,
  SidebarMinimizeDirective,
  SidebarOffCanvasCloseDirective,
  BrandMinimizeDirective,
  SidebarToggleDirective,
  MobileSidebarToggleDirective,
} from './directives';
const LAYOUT_DIRECTIVES = [
  AsideToggleDirective,
  NavDropdownDirective,
  NavDropdownToggleDirective,
  ReplaceDirective,
  SidebarMinimizeDirective,
  SidebarOffCanvasCloseDirective,
  BrandMinimizeDirective,
  SidebarToggleDirective,
  MobileSidebarToggleDirective,
];

// Import 3rd party components
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Import Own Resources
import { MenuService } from 'src/app/services/menu.service';
import { SafeResourceModule } from 'src/app/shared-modules/pipes/safe-resource/safe-resource.module';

@NgModule({
  declarations: [
    FullLayoutComponent,
    ...LAYOUT_COMPONENTS,
    ...LAYOUT_DIRECTIVES,
  ],
  imports: [
    CommonModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    SafeResourceModule
  ],
  exports: [
    FullLayoutComponent
  ],
  providers: [
    MenuService
  ]
})
export class FullLayoutModule { }
