import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { SessionService } from 'src/app/state/session.service';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
})
export class AppHeaderComponent implements OnInit {
  @Input() public imgUrl: string;
  @Input() public appLogo: string;
  @Input() public user: any;
  @Input() public notifications: any[];
  @Input() public messages: any[];

  public subscription: Subscription;
  public $menuData;

  constructor(
    private menuService: MenuService,
    private session: SessionService
    ) {
    this.$menuData = this.menuService.currentMenu;
  }

  public logOut() {
    this.session.logout();
  }

  public ngOnInit() {
    document.querySelector('app-root')!.classList.add('aside-menu-hidden');
  }

}
