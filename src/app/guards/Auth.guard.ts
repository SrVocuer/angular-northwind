import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ToasterService } from '@app-toaster';
import { AadService } from '../services/idps/aad.service';
import { SessionService } from '../state/session.service';
import { ConfigService } from '../services/config.service';
import { SecureStorageService } from '../services/secure-storage.service';
import { MsalGuard } from '@azure/msal-angular';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private http: HttpClient,
    private session: SessionService,
    private storageService: SecureStorageService,
    private appSettings: ConfigService,
    private aadService: AadService,
    private toasterService: ToasterService,
    private msalGuard: MsalGuard
  ) {}

  // - REGLAS DE AUTENTICACIÓN

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {

    // Si se definió algún IdP no existe sesión
    const idpSetted = typeof localStorage.getItem('idp') == "string";
    if (!idpSetted) {
      return this.session.logout();
    }

    // Si el IdP seleccionado es AAD, se delega la lógica a msalGuard
    const idpSelected = this.storageService.getItem('idp');
    if (idpSelected === 'AAD') {
      const $msalGuard = this.msalGuard.canActivate(route, state);
      const msalResult = await firstValueFrom($msalGuard)
      if (!msalResult) {
        return this.interrupt('La sesión expiró')
      }
    }

    // Se obtienen los datos de sesión
    return new Promise((resolve) => {
      this.session.currentUser.subscribe(
        currentUser => {
          if (currentUser != null) {
            resolve(true)
          } else if (idpSelected !== 'AAD') {
            this.fetchApiProfile()
          } else if (idpSelected === 'AAD') {
            this.fetchAadProfile()
          }
        }
      );
    })

  }

  // - LÓGICA DE CIERRE DE SESIÓN

  private interrupt(error: any): boolean {
    if (typeof error === "string") {
      this.toasterService.showError(error, 'Session Error')
    }
    this.session.logout();
    return false;
  }

  // - MÉTODOS DE OBTENCIÓN DE DATOS DE SESIÓN

  private fetchApiProfile(): void {
    const apiUrl = this.appSettings.getConfig().webApiBaseUrl;
    this.http.get(`${apiUrl}/me`).subscribe({
      next: (me: any) => {
        this.session.setCurrentUser(me.data);
      },
      error: (e) => this.interrupt(e)
    })
  }

  private fetchAadProfile(): void {
    this.aadService.fetchProfile().subscribe({
      next: profile => {
        this.session.setCurrentUser(profile)
      },
      error: (e) => this.interrupt(e)
    })
  }

}