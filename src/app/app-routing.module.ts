import { environment } from './../environments/environment';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router, NavigationEnd } from '@angular/router';

import { AuthGuard } from './guards/Auth.guard';
import { FullLayoutComponent } from './containers/full-layout';
import { SimpleLayoutComponent } from './containers/simple-layout/simple-layout.component';

const isIframe = window !== window.parent && !window.opener;

export const APP_ROUTES: Routes = [
  {
    path: 'login',
    component: SimpleLayoutComponent,
    data: { title: 'Login' },
    loadChildren: () => import('./modules/login/login.module').then((m) => m.LoginModule),
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [AuthGuard],
    data: { title: 'Home' },
    children: [
      {
        path: 'dashboard',
        data: { title: 'Dashboard' },
        loadChildren: () => import('./modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'resources',
        data: { title: 'Resources' },
        loadChildren: () => import('./modules/resources/resources.module').then((m) => m.ResourcesModule),
      },
      {
        path: 'example',
        data: { title: 'Examples' },
        loadChildren: () => import('./modules/example/example.module').then((m) => m.ExampleModule),
      },
      {
        path: 'category',
        data: { title: 'Categorias' },
        loadChildren: () => import('./modules/category/category.module').then((m) => m.CategoryModule),
      },
      {
        path: 'employees',
        data: { title: 'Employee' },
        loadChildren: () => import('./modules/employee/employee.module').then((m) => m.EmployeeModule),
      },
      {
        path: 'suppliers',
        data: { title: 'Suppliers' },
        loadChildren: () => import('./modules/suppliers/suppliers.module').then((m) => m.SuppliersModule),
      },
      {
        path: 'products',
        data: { title: 'Products' },
        loadChildren: () => import('./modules/products/products.module').then((m) => m.ProductsModule),
      },
      {
        path: 'clientes',
        data: { title: 'Clientes' },
        loadChildren: () => import('./modules/clientes/clientes.module').then((m) => m.ClientesModule),
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, { initialNavigation: !isIframe ? 'enabledNonBlocking' : 'disabled' })],
  exports: [RouterModule],
})
export class AppRoutingModule {
  constructor(private router: Router) {
    if (environment.production) {
      this.router.errorHandler = (_error: any) => {
        this.router.navigate(['/dashboard']); // or redirect to default route
      };
    } else {
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          console.log(event);
        }
      });
    }
  }
}
