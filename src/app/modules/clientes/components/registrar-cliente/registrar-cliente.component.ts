import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-cliente',
  templateUrl: './registrar-cliente.component.html',
  styleUrls: ['./registrar-cliente.component.scss']
})
export class RegistrarClienteComponent implements OnInit {

  clientes :FormGroup;
  url = "clientes/";
  numberPattern = '^[0-9]*$';
  
  constructor(
    private service: ApiService,
    private router: Router,
    public formulario: FormBuilder
  ) { 

    this.clientes = this.formulario.group({
      companyName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      contactName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      contactTitle: new FormControl("", [Validators.required, Validators.minLength(4)]),
      address: new FormControl("", [Validators.required, Validators.minLength(10)]),
      country: new FormControl("", [Validators.required, Validators.minLength(4)]),
      city: new FormControl("", [Validators.required, Validators.minLength(4)]),
      region: new FormControl("", [Validators.required, Validators.minLength(4)]),
      postalCode: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      phone: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      fax: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
    });

  }

  ngOnInit(): void {
  }

  guardar(){
    if (this.clientes.valid) {
      this.service.registrar(this.url, this.clientes.value).subscribe({
        complete: () => {
          Swal.fire(
            'Cliente Registrado',
            '¡Nueva empleado registrado con exito!',
            'success'
          );
          this.router.navigateByUrl("clientes");
        },
        error: () => {

          Swal.fire(
            'Error al Registrar',
            '¡Ups! Ocurrio un error',
            'error'
          );
        }

      });
    } else {
      Swal.fire(
        'Revisa tu formulario',
        'Error en tu formulario',
        'error'
      )
    }
    

  }

  err = {
    "company": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 4."
      }
    ],
    "address": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      },
      {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 60."
      }
    ],
    "country": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 4."
      }, {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 20."
      }
    ],
    "postal": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se permiten números"
      },
    ]
  };
}
