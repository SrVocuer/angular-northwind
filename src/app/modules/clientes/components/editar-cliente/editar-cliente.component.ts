import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.scss']
})
export class EditarClienteComponent implements OnInit {
  clientesEdit :FormGroup;
  url = "clientes/";
  numberPattern = '^[0-9]*$';
  id;

  constructor(
    private service: ApiService,
    private router: Router,
    public formulario: FormBuilder,
    private activeRoute: ActivatedRoute,
  ) {
    this.clientesEdit = this.formulario.group({
      companyName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      contactName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      contactTitle: new FormControl("", [Validators.required, Validators.minLength(4)]),
      address: new FormControl("", [Validators.required, Validators.minLength(10)]),
      country: new FormControl("", [Validators.required, Validators.minLength(4)]),
      city: new FormControl("", [Validators.required, Validators.minLength(4)]),
      region: new FormControl("", [Validators.required, Validators.minLength(4)]),
      postalCode: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      phone: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      fax: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
    });
   }

  ngOnInit(): void {
    this.id = this.activeRoute.snapshot.paramMap.get('id');
    this.rellenarForm();

  }

  editar(){
    if (this.clientesEdit.valid) {
      this.service.editar(this.url, this.id, this.clientesEdit.value).subscribe({
        complete: () => {
          Swal.fire(
            'Cliente Actualizado',
            '¡Cliente actualizado con exito!',
            'success'
          );
          this.router.navigateByUrl("clientes");
        },
        error: (d) => {

          Swal.fire(
            'Error al Actualizar',
            '¡Ups! Ocurrio un error',
            'error'
          );
        }

      });
    } else {
      Swal.fire(
        'Revisa tu formulario',
        'Error en tu formulario',
        'error'
      )
    }
    
  }
  rellenarForm() {

    this.service.consultar(this.url, this.id).subscribe(data =>{
      this.clientesEdit.setValue({
        companyName: data.data.companyName,
        contactName: data.data.contactName,
        contactTitle: data.data.contactTitle,
        address: data.data.address,
        country: data.data.country,
        city: data.data.city,
        region: data.data.region,
        postalCode: data.data.postalCode,
        phone: data.data.phone,
        fax: data.data.fax,
      });
    });
  }

  err = {
    "company": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 4."
      }
    ],
    "address": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      },
      {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 60."
      }
    ],
    "country": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 4."
      }, {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 20."
      }
    ],
    "postal": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se permiten números"
      },
    ]
  };
}
