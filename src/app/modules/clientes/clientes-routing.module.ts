import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './clientes.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { RegistrarClienteComponent } from './components/registrar-cliente/registrar-cliente.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: ClientesComponent,
    data: { title: 'Clientes' },
  },
  {
    path: 'registrar',
    component: RegistrarClienteComponent,
    data: { title: 'Registrar' },
  },
  {
    path: 'editar/:id',
    component: EditarClienteComponent,
    data: { title: 'Editar' },
  }
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class ClientesRoutingModule { }
