import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  url ="clientes/";
  clientes = [];
  constructor(
    private service: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.consultar();
  }

  consultar(){
    this.service.consultar(this.url, null).subscribe(response =>{
      this.clientes = response.data;
    });
  }

  borrarCliente(id){
    Swal.fire({
      title: '¿Estás seguro de eliminar?',
      text: "Recuerda, no se puede revertir esto.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.service.eliminar(this.url, id).subscribe({
          complete: () => {
            Swal.fire(
              'Cliente',
              '¡Se ha sido eliminado!',
              'success'
            );

            this.consultar();
          },
          error: () => {
            Swal.fire(
              'Error al Eliminar',
              '¡Ups!,No se pudo eliminar.',
              'error'
            );
          }
        });
      }

    });
  }
}
