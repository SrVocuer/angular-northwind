import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-employees',
  templateUrl: './registrar-employees.component.html',
  styleUrls: ['./registrar-employees.component.scss']
})
export class RegistrarEmployeesComponent implements OnInit {

  empleados: FormGroup;
  url = "empleados/";
  select =[];


  constructor(
    public formulario: FormBuilder,
    private service: ApiService,
    private router: Router,
  ) {

    this.empleados = this.formulario.group({
      firstName: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      lastName: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      address: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      country: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      city: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      region: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      postalCode: ['', new FormControl([Validators.compose([Validators.required, Validators.pattern('[0-9]+')])])],
      homePhone: ['', new FormControl([Validators.compose([Validators.required, Validators.pattern('[0-9]+')])])],
      extension: ['', new FormControl([Validators.compose([Validators.required, Validators.pattern('[0-9]+')])])],
      birthDate: ['', new FormControl([Validators.compose([Validators.required])])],
      hireDate: ['', new FormControl([Validators.compose([Validators.required])])],
      title: ['', new FormControl([Validators.compose([Validators.required, Validators.maxLength(50)])])],
      titleOfCourtesy: ['', new FormControl([Validators.compose([Validators.required, Validators.maxLength(10)])])],
      notes: ['', new FormControl([Validators.compose([Validators.required, Validators.maxLength(75)])])],
      reportsTo: ['', new FormControl([Validators.compose([Validators.pattern('[0-9]+')])])],
      photo: '',
    });

  }

  ngOnInit(): void {
    this.consultar();
    //console.log(this.select);
    
  }

  enviarEmployee(): any {

    console.log(this.empleados.value);

    if (this.empleados.valid) {
      this.service.registrar(this.url, this.empleados.value).subscribe({
        complete: () => {
          Swal.fire(
            'Empledo Registrado',
            '¡Nueva empleado registrado con exito!',
            'success'
          );
          this.router.navigateByUrl("employees");
        },
        error: (e) => {

          Swal.fire(
            'Error al Registrar',
            '¡Ups! Ocurrio un error' ,
            'error'
          );
        }
        
      });
    }


  }

  consultar(){
     this.service.consultar(this.url, null).subscribe(
      {
        next: (s) =>{
          //console.log(s.data);
          return this.select = s.data;
        }
      }
    );
  }

}
