import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-editar-employees',
  templateUrl: './editar-employees.component.html',
  styleUrls: ['./editar-employees.component.scss']
})
export class EditarEmployeesComponent implements OnInit {

  editEmpleados: FormGroup;
  url = "empleados/";
  idEmployee: any;
  select = [];

  constructor(
    public formulario: FormBuilder,
    private service: ApiService,
    private router: Router,
    private activeRoute: ActivatedRoute,

  ) {

    this.editEmpleados = this.formulario.group({
      firstName: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      lastName: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      address: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      country: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      city: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      region: ['', new FormControl([Validators.compose([Validators.required, Validators.minLength(4)])])],
      postalCode: ['', new FormControl([Validators.compose([Validators.required, Validators.pattern('[0-9]+')])])],
      homePhone: ['', new FormControl([Validators.compose([Validators.required, Validators.pattern('[0-9]+')])])],
      extension: ['', new FormControl([Validators.compose([Validators.required, Validators.pattern('[0-9]+')])])],
      birthDate: ['', new FormControl([Validators.compose([Validators.required])])],
      hireDate: ['', new FormControl([Validators.compose([Validators.required])])],
      title: ['', new FormControl([Validators.compose([Validators.required, Validators.maxLength(50)])])],
      titleOfCourtesy: ['', new FormControl([Validators.compose([Validators.required, Validators.maxLength(10)])])],
      notes: ['', new FormControl([Validators.compose([Validators.required, Validators.maxLength(75)])])],
      reportsTo: ['', new FormControl([Validators.compose([Validators.pattern('[0-9]+')])])],
      photo: '',
    });
  }

  ngOnInit(): void {
    this.idEmployee = this.activeRoute.snapshot.paramMap.get('id');

    this.rellenarForm(this.idEmployee);

    this.service.consultar(this.url, null).subscribe(data =>{
      this.select = data.data;
    });

  }

  editarEmployee() {

  }

  rellenarForm(num) {
    this.service.consultar(this.url, num).subscribe({   
      next: (data) => { // console.log(data.data);
        
        this.editEmpleados = this.formulario.group({
          firstName: data.data.firstName,
          lastName: data.data.lastName,
          address: data.data.address,
          country: data.data.country,
          city: data.data.city,
          region: data.data.region,
          postalCode: data.data.postalCode,
          homePhone: data.data.homePhone,
          extension: data.data.extension,
          birthDate: data.data.birthDate,
          hireDate: data.data.hireDate,
          title: data.data.title,
          titleOfCourtesy: data.data.titleOfCourtesy,
          notes: data.data.notes,
          reportsTo: data.data.reportsTo,
          photo: '',
        });
      }

    });
  }
}
