import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutingModule } from './employees-routing.module';
import { RegistrarEmployeesComponent } from './components/registrar-employees/registrar-employees.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EditarEmployeesComponent } from './components/editar-employees/editar-employees.component';


@NgModule({
  declarations: [
    RegistrarEmployeesComponent,
    EditarEmployeesComponent
  ],
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    ReactiveFormsModule, 
    FormsModule, 
    HttpClientModule
  ]
})
export class EmployeesModule { }
