import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarEmployeesComponent } from './components/editar-employees/editar-employees.component';
import { RegistrarEmployeesComponent } from './components/registrar-employees/registrar-employees.component';
import { EmployeesComponent } from './employees.component';


const APP_ROUTES: Routes = [
  {
    path: '',
    data: { title: 'Employee', },
    component: EmployeesComponent
  },

  {
    path: 'registrar',
    data: { title: 'New' },
    component: RegistrarEmployeesComponent
  },
  {
    path: 'editar/:id',
    data: { title: 'Edit' },
    component: EditarEmployeesComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
