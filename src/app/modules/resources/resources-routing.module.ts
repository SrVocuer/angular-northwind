import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourcesComponent } from './resources.component';

const APP_ROUTES: Routes = [
    {
        path: '',
        component: ResourcesComponent,
        data: {
            title: 'Components',
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(APP_ROUTES)],
    exports: [RouterModule],
})
export class ResourcesRoutingModule { }
