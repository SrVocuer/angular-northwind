import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CaptchaApiService } from '@app-recaptcha';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public authMechanisms = environment.authMechanisms;
  public loginSelected!: string;
  public loginUnique!: string;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthenticationService,
    private router: Router,
    private recaptcha: CaptchaApiService
  ) {

    let loginsWithForm = []
    let loginsWithButton = []

    for (const [idp, isEnabled] of Object.entries(this.authMechanisms)) {
      if (isEnabled === true && idp !== 'authCode') {
        loginsWithButton.push(idp)
        if (idp !== 'azureAD') {
          loginsWithForm.push(idp)
        }
      }
    }

    if (loginsWithButton.length === 1 && loginsWithForm.length === 1) {
      this.loginUnique = this.loginSelected = loginsWithForm[0];
    }

  }

  public ngOnInit() {
    this.verify()
  }

  public verify() {
    const idpSetted = typeof localStorage.getItem('idp') == "string";
    this.route.queryParams.subscribe(({token}) => {
      if (token) {
        this.auth.login('authCode', token);
      } else if (idpSetted) {
        this.router.navigate(['']);
      }
    });
  }

  public async login(authMechanism: string, credentials: any = {}) {
    try {
      if (environment.authMechanisms.colaboradorDigital) {
        const captchaToken: string = await this.recaptcha.getToken('createAccount');
        this.auth.login(authMechanism, credentials, captchaToken)
      } else {
        this.auth.login(authMechanism, credentials)
      }
    } catch (error) {
      console.log(error);
    }
  }
}

