import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RecaptchaV3Module } from '@app-recaptcha';

import { LoginComponent } from './login.component';
import { LoginCDigitalComponent } from './components/login-cdigital/login-cdigital.component';

import { AadService } from 'src/app/services/idps/aad.service';
import { SsoService } from 'src/app/services/idps/sso.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CustomService } from 'src/app/services/idps/custom.service';
import { LoginCustomComponent } from './components/login-custom/login-custom.component';

// import { HuellaModule, HuellaService } from '@oc/ngx-huella';
// import { LoginHuellaComponent } from './components/login-huella/login-huella.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: LoginComponent,
    data: {
      title: 'Login Page',
    },
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RecaptchaV3Module,
    // HuellaModule,
    RouterModule.forChild(APP_ROUTES)
  ],
  declarations: [
    LoginComponent,
    LoginCDigitalComponent,
    LoginCustomComponent,
    // LoginHuellaComponent
  ],
  providers: [
    AadService,
    SsoService,
    CustomService,
    AuthenticationService,
    // HuellaService
  ]
})
export class LoginModule {}