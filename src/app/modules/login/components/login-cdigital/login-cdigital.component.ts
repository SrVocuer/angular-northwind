import { Component, Output, Input, EventEmitter } from "@angular/core";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'login-cdigital',
  templateUrl: './login-cdigital.component.html',
  styleUrls: ['./../../login.component.scss']
})
export class LoginCDigitalComponent {

  @Output() credentials = new EventEmitter<any>();
  @Output() goback = new EventEmitter<any>();
  @Input() allowBack = false;

  public back() { this.goback.emit() }
  public login(f: NgForm) { this.credentials.emit(f.form.value) }

}