import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleComponent } from './example.component';
import { ExampleLoaderComponent } from './components/example-loader/example-loader.component';
import { ExampleSessionComponent } from './components/example-session/example-session.component';
import { ExampleStorageComponent } from './components/example-storage/example-storage.component';
import { ExampleToasterComponent } from './components/example-toaster/example-toaster.component';

import { LoaderModule } from '@app-loader';

describe('ExampleComponent', () => {
  let component: ExampleComponent;
  let fixture: ComponentFixture<ExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LoaderModule],
      declarations: [
        ExampleComponent,
        ExampleSessionComponent,
        ExampleStorageComponent,
        ExampleLoaderComponent,
        ExampleToasterComponent
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
