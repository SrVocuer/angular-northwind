import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExampleComponent } from './example.component';

const APP_ROUTES: Routes = [
    {
        path: '',
        component: ExampleComponent,
        data: {
            title: 'Example',
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(APP_ROUTES)],
    exports: [RouterModule],
})
export class ExampleRoutingModule { }
