import { Component } from '@angular/core';
import { LoaderService } from '@app-loader';

@Component({
  selector: 'app-example-loader',
  templateUrl: './example-loader.component.html'
})
export class ExampleLoaderComponent {

  public toggleLoader = false;
  public fullOverlay = false;

  constructor (private loaderService: LoaderService) { }

  showFullLoader() {
    this.loaderService.setVisibleState(true)
    setTimeout(() => {
      this.loaderService.setVisibleState(false)
    }, 2000);
  }

}
