import { Component } from '@angular/core';
import { ToasterService } from '@app-toaster';

@Component({
  selector: 'app-example-toaster',
  templateUrl: './example-toaster.component.html'
})
export class ExampleToasterComponent {

  constructor(private toasterService: ToasterService) { }

  toastMessage(variant: string) {
    const title = 'Mensaje de prueba'
    let message = `No podrás enviar un mensaje nuevo hasta cerrar el anterior. `
    message += `Esto evitará un bucle de envío de mensajes si llegara a ocurrir un error iterativo.`
    message += `<br><strong><a href="https://sites.google.com/coppel.com/developers/frameworks/webclient/novedades?authuser=0" target="_blank">Consultar Componentes</a></strong>`
    switch (variant) {
      case 'Error':
        this.toasterService.showError(message, title)
        break;
      case 'Warn':
        this.toasterService.showWarn(message, title)
        break;
      case 'Info':
        this.toasterService.showInfo(message, title)
        break;
      case 'Approval':
        this.toasterService.showApproval(message, title)
        break;
      case 'Mesage':
        this.toasterService.showMesage(message, title)
        break;
      default:
        this.toasterService.showMesage(message, title)
        break;
    }
  }

}
