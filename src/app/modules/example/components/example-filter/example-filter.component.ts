import { Component, OnInit } from '@angular/core';

export interface TaskInterface {
  task: string,
  status: string | number,
  asignee: {
    id: number,
    name: string
  }
}

const templateTasks = ["Task Test", "Task Clean", "Task Upload"]
const templateNames = ["Jhon", "Jess", "Dahl"]
const templateStatus = ["To Do", "Doing", "Done"]
const getRandomInt = (max = 3) => Math.floor(Math.random() * max);

@Component({
  selector: 'app-example-filter',
  templateUrl: './example-filter.component.html',
  styles: [" #example { display: grid; grid-template-columns: 1fr 1fr; } "]
})
export class ExampleFilterComponent implements OnInit {

  public tasks: TaskInterface[];
  public statusFilter = 'Done';

  constructor() { }

  ngOnInit(): void {
    this.fillRandomTasks()
  }

  public fillRandomTasks() {
    this.tasks = []
    for (let index = 0; index <= 4; index++) {
      this.tasks.push({
        task: `${templateTasks[getRandomInt()]} ${index}`,
        status: templateStatus[getRandomInt()],
        asignee: {
          id: index,
          name: templateNames[getRandomInt()]
        }
      })
    }
  }

}
