import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/state/session.service';

@Component({
  selector: 'app-example-session',
  templateUrl: './example-session.component.html'
})
export class ExampleSessionComponent {

  public profile: Observable<any>;

  constructor(private session: SessionService) {
    this.profile = this.session.currentUser;
  }
  
  public modifySession() {
    this.session.setCurrentUser({
      modified: true
    })
  }

}
