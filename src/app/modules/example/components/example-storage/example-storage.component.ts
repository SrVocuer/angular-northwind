import { Component, OnInit } from '@angular/core';
import { SecureStorageService } from 'src/app/services/secure-storage.service';

@Component({
  selector: 'app-example-storage',
  templateUrl: './example-storage.component.html'
})
export class ExampleStorageComponent implements OnInit {

  public storage: any;

  constructor(private secureStorage: SecureStorageService) { }

  ngOnInit(): void {
    this.getSecureStorageTest()
  }

  private getSecureStorageTest() {
    this.storage = {
      idpEncrypted: localStorage.getItem('idp'),
      idpDecrypted: this.secureStorage.getItem('idp'),
      tokenEncrypted: localStorage.getItem('token'),
      tokenDecrypted: this.secureStorage.getItem('token'),
      empty: this.secureStorage.getItem('empty')
    }
  }

  public setSecureStorageTest() {
    this.secureStorage.setItem('test', 'test')
    this.storage.nuevoEncrypted = localStorage.getItem('test')
    this.storage.nuevoDecrypted = this.secureStorage.getItem('test')
  }
}
