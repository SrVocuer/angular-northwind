import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExampleRoutingModule } from './example-routing.module';

import { ExampleComponent } from './example.component';
import { ExampleSessionComponent } from './components/example-session/example-session.component';
import { ExampleStorageComponent } from './components/example-storage/example-storage.component';
import { ExampleLoaderComponent } from './components/example-loader/example-loader.component';
import { ExampleToasterComponent } from './components/example-toaster/example-toaster.component';
import { ExampleFilterComponent } from './components/example-filter/example-filter.component';

import { LoaderModule } from '@app-loader';
import { FilterByPropModule } from 'src/app/shared-modules/pipes/filter-by-prop/filter-by-prop.pipe.module';

@NgModule({
  declarations: [
    ExampleComponent,
    ExampleSessionComponent,
    ExampleStorageComponent,
    ExampleLoaderComponent,
    ExampleToasterComponent,
    ExampleFilterComponent,
  ],
  imports: [
    CommonModule,
    LoaderModule,
    ExampleRoutingModule,
    FilterByPropModule,
  ]
})
export class ExampleModule { }
