import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-categoria',
  templateUrl: './registrar-categoria.component.html',
  styleUrls: ['./registrar-categoria.component.scss']
})
export class RegistrarCategoriaComponent implements OnInit {

  formularioDeCategorias: FormGroup;
  url = "categorias/";
  formData : FormData;

  constructor(
    public formulario: FormBuilder,
    private service: ApiService,
    private router: Router,
  ) {

  }

  ngOnInit(): void {

    this.formularioDeCategorias = this.formulario.group({
      categoryName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      description: new FormControl("", [Validators.required, Validators.minLength(10)]),
      picture: [''],
    });



  }

  enviarDatos(): any {

    // const formData = new FormData();

    // Object.keys(this.formularioDeCategorias.controls).forEach(key => {

    //   const controlValue = this.formularioDeCategorias.get(key)?.value?.toString();
    //     console.log(key, controlValue);        
    //    formData.append(key, controlValue);

    // });
    // console.log(formData);
    this.service.registrar(this.url, this.formularioDeCategorias.value).subscribe({

      complete: () => {
        Swal.fire(
          'Categoria Registrada',
          '¡Nueva categoria registrada con exito!',
          'success'
        );
        this.router.navigateByUrl("category");
      },

      error: () => {

        Swal.fire(
          'Error al Registrar',
          '¡Nueva categoria no registrada!',
          'error'
        );
      }
    });
  }

  get categoryName(): FormControl {
    return this.formularioDeCategorias.get('categoryName') as FormControl;
  }

  get description(): FormControl {
    return this.formularioDeCategorias.get('description') as FormControl;
  }

  err = {

    "categoryName": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 3."
      }
    ],
    "description": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      }
    ]};

}
