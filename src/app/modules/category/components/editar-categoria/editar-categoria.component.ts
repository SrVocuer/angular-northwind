import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.scss']
})
export class EditarCategoriaComponent implements OnInit {

  idEditar:any;
  formularioDeCategorias: FormGroup;
  url = "categorias/";

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private service: ApiService,
    public formulario: FormBuilder,
  ) { 

    this.idEditar = this.activeRoute.snapshot.paramMap.get('id');
    this.service.consultar(this.url,this.idEditar).subscribe(
      respuesta =>{
        
        this.formularioDeCategorias.setValue({
          categoryName: respuesta.data.categoryName,
          description: respuesta.data.description,
          picture: respuesta.data.picture,
        });
      }
    );

    this.formularioDeCategorias = this.formulario.group({
      categoryName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      description: new FormControl("", [Validators.required, Validators.minLength(10)]),
      picture: [''],
    });
    
  }

  ngOnInit(): void {
  }

  actualizarDatos():any{
    
    this.service.editar(this.url, this.idEditar, this.formularioDeCategorias.value).subscribe({
      complete: () => {
        Swal.fire(
          'Categoria Actualizada',
          '¡Categoria Actualizada con exito!',
          'success'
        );
        this.router.navigateByUrl("category");
      },

      error: (err:any) => {

        Swal.fire(
          'Error al Registrar',
          '¡No se pudo Actualizar la Categoria! :'+ err.status,
          'error'
        );

      }

    });


  }
  get categoryName(): FormControl {
    return this.formularioDeCategorias.get('categoryName') as FormControl;
  }

  get description(): FormControl {
    return this.formularioDeCategorias.get('description') as FormControl;
  }

  err = {

    "categoryName": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 3."
      }
    ],
    "description": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      }
    ]};

}
