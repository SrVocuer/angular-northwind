import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-categorias',
  templateUrl: './registrar-categorias.component.html',
  styleUrls: ['./registrar-categorias.component.scss']
})
export class RegistrarCategoriasComponent implements OnInit {

  formularioDeCategorias: FormGroup;
  url = "categorias/";

  constructor(
    public formulario: FormBuilder,
    private service: ApiService,
    private router: Router,
  ) {

  }

  ngOnInit(): void {

    this.formularioDeCategorias = this.formulario.group({
      categoryName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      description: new FormControl("", [Validators.required, Validators.minLength(10)]),
      picture: [''],
    });



  }

  enviarDatos(): any {

    this.service.registrar(this.url, this.formularioDeCategorias.value,).subscribe({

      complete: () => {
        Swal.fire(
          'Categoria Registrada',
          '¡Nueva categoria registrada con exito!',
          'success'
        );
        this.router.navigateByUrl("category");
      },

      error: () => {

        Swal.fire(
          'Error al Registrar',
          '¡Nueva categoria no registrada!',
          'error'
        );

      }

    });
  }

  get categoryName(): FormControl {
    return this.formularioDeCategorias.get('categoryName') as FormControl;
  }

  get description(): FormControl {
    return this.formularioDeCategorias.get('description') as FormControl;
  }

  erros = [
    { "required": "Campo Obligadorio" },
    { "minLength": "El tamaño minimo es de 4 carcteres" },
    { "number": "Solo se permiten valores númericos"}
  ];
}
