import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-categorias',
  templateUrl: './editar-categorias.component.html',
  styleUrls: ['./editar-categorias.component.scss']
})
export class EditarCategoriasComponent implements OnInit {

  idEditar:any;
  formularioDeCategorias: FormGroup;
  url = "categorias/";

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private service: ApiService,
    public formulario: FormBuilder,
  ) { 

    this.idEditar = this.activeRoute.snapshot.paramMap.get('id');
    this.service.consultar(this.url,this.idEditar).subscribe(
      respuesta =>{
        
        this.formularioDeCategorias.setValue({
          categoryName: respuesta.data.categoryName,
          description: respuesta.data.description,
          picture: respuesta.data.picture,
        });
      }
    );

    this.formularioDeCategorias = this.formulario.group({

      categoryName: [''],
      description: [''],
      picture: [''],

    });
    
  }

  ngOnInit(): void {
  }

  actualizarDatos():any{
    //console.log(this.idEditar);
    this.service.editar(this.url, this.idEditar, this.formularioDeCategorias.value).subscribe({
      complete: () => {
        Swal.fire(
          'Categoria Actualizada',
          '¡Categoria Actualizada con exito!',
          'success'
        );
        this.router.navigateByUrl("category");
      },

      error: (err:any) => {

        Swal.fire(
          'Error al Registrar',
          '¡No se pudo Actualizar la Categoria! :'+ err.status,
          'error'
        );

      }

    });


  }

}
