import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './category.component';
import { EditarCategoriaComponent } from './components/editar-categoria/editar-categoria.component';
import { RegistrarCategoriaComponent } from './components/registrar-categoria/registrar-categoria.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: CategoryComponent,
    data: { title: 'Categorias' },
  },
  {
    path: 'registrar',
    data: { title: 'Registrar' },
    component: RegistrarCategoriaComponent,
  },
  {
    path: 'editar/:id',
    data: { title: 'Editar' },
    component: EditarCategoriaComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
