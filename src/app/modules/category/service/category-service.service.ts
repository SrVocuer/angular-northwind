import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from './Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryServiceService {

  API: string = "http://localhost:8080/api/v1/";


  constructor(private clienteHttp: HttpClient) { }

  agregarCategoria(datos: Category, url:string): Observable<any> {
    return this.clienteHttp.post(this.API+url, datos);
  }

  consultaCategorias(url){
    return  this.clienteHttp.get<any>(this.API+url);
  }

  
}
