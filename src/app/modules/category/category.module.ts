import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegistrarCategoriaComponent } from './components/registrar-categoria/registrar-categoria.component';
import { EditarCategoriaComponent } from './components/editar-categoria/editar-categoria.component';



@NgModule({
  declarations: [
    RegistrarCategoriaComponent,
    EditarCategoriaComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ]
})
export class CategoryModule { }
