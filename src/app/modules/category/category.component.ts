import { Component, OnInit, ViewChild } from '@angular/core';

import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})

export class CategoryComponent implements OnInit {

  Categoria = [];
  url = "categorias/";

  constructor(
    private service: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.service.consultar(this.url, null).subscribe(response => {
      console.log(response.data);

      this.Categoria = response.data;
    });

  }
  borrarCategoria(id: number, iControl) {
    Swal.fire({
      title: '¿Estás seguro de eliminar?',
      text: "Recuerda, no se puede revertir esto.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.service.eliminar(this.url, id).subscribe({
          complete: () => {
            Swal.fire(
              'Elminado',
              '¡Categoria ha sido eliminada!',
              'success'
            );
            this.Categoria.splice(iControl, 1);
            this.router.navigateByUrl('category');
          },
          error: () => {
            Swal.fire(
              'Error al Eliminar',
              '¡No se pudo eliminar esta categoira, compruebe que no esté en uso!',
              'error'
            );
          }
        });
      }

    });



  }


  // columns = [
  //   { prop: 'categoryID' },
  //   { prop: 'categoryName' },
  //   { prop: 'categoryName' },
  //   { prop: 'categoryName' },
  //   { name: 'Acciones' }

  // ];



}
