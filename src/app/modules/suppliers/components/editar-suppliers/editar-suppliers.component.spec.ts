import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarSuppliersComponent } from './editar-suppliers.component';

describe('EditarSuppliersComponent', () => {
  let component: EditarSuppliersComponent;
  let fixture: ComponentFixture<EditarSuppliersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarSuppliersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditarSuppliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
