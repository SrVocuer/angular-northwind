import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarSuppliersComponent } from './registrar-suppliers.component';

describe('RegistrarSuppliersComponent', () => {
  let component: RegistrarSuppliersComponent;
  let fixture: ComponentFixture<RegistrarSuppliersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarSuppliersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrarSuppliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
