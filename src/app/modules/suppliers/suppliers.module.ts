import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuppliersRoutingModule } from './suppliers-routing.module';
import { RegistrarSuppliersComponent } from './components/registrar-suppliers/registrar-suppliers.component';
import { EditarSuppliersComponent } from './components/editar-suppliers/editar-suppliers.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    RegistrarSuppliersComponent,
    EditarSuppliersComponent
  ],
  imports: [
    CommonModule,
    SuppliersRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ]
})
export class SuppliersModule { }
