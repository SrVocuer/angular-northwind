import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {
  modal:BsModalRef;
  Suppliers = [];
  url ="proveedores/";

  constructor(
    private service: ApiService,
    private servicio:BsModalService,
  ) { }

  ngOnInit(): void {
    this.consultar();
   
  }

  consultar(){
    this.service.consultar(this.url, null).subscribe(response =>{   
      this.Suppliers = response.data;
    });
  }

  borrarProveedor(id){
    Swal.fire({
      title: '¿Estás seguro de eliminar?',
      text: "Recuerda, no se puede revertir esto.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.service.eliminar(this.url, id).subscribe({
          complete: () => {
            Swal.fire(
              'Proveedor',
              '¡Proveedor ha sido eliminado!',
              'success'
            );
            this.consultar();
          },
          error: () => {
            Swal.fire(
              'Error al Eliminar',
              '¡No se pudo eliminar éste empleado, compruebe que no esté en uso!',
              'error'
            );
          }
        });
      }

    });
  }

  abrirModal(template: TemplateRef<any>){
    this.modal = this.servicio.show(template);
  }
}
