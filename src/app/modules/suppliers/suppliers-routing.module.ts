import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarSuppliersComponent } from './components/editar-suppliers/editar-suppliers.component';
import { RegistrarSuppliersComponent } from './components/registrar-suppliers/registrar-suppliers.component';

import { SuppliersComponent } from './suppliers.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: SuppliersComponent,
    data: { title: 'Suppliers' },
  },
  {
    path: 'registrar',
    component: RegistrarSuppliersComponent,
    data: { title: 'New Suplier' }
  },
  {
    path: 'editar/:id',
    component: EditarSuppliersComponent,
    data: { title: 'New Suplier' },
  },

];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class SuppliersRoutingModule { }
