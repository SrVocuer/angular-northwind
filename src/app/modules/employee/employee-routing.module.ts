import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarEmpleadoComponent } from './components/editar-empleado/editar-empleado.component';
import { RegistrarEmpleadoComponent } from './components/registrar-empleado/registrar-empleado.component';
import { EmployeeComponent } from './employee.component';


const APP_ROUTES: Routes = [
  {
    path: '',
    data: { title: 'Employee', },
    component: EmployeeComponent
  },

  {
    path: 'registrar',
    data: { title: 'New' },
    component: RegistrarEmpleadoComponent
  },
  {
    path: 'editar/:id',
    data: { title: 'Edit' },
    component: EditarEmpleadoComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
