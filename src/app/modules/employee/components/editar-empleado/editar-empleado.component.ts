import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.scss']
})
export class EditarEmpleadoComponent implements OnInit {


  editEmpleados: FormGroup;
  url = "empleados/";
  idEmployee: any;
  numberPattern = '^[0-9]*$';
  select = [];

  constructor(
    public formulario: FormBuilder,
    private service: ApiService,
    private router: Router,
    private activeRoute: ActivatedRoute,

  ) {  }

  ngOnInit(): void {
   
    this.idEmployee = this.activeRoute.snapshot.paramMap.get('id');

    this.rellenarForm(this.idEmployee);

    this.service.consultar(this.url, null).subscribe(data => {
      this.select = data.data;
    });

    this.editEmpleados = this.formulario.group({
      firstName: new FormControl("", [Validators.required, Validators.minLength(3)]),
      lastName: new FormControl("", [Validators.required, Validators.minLength(3)]),
      address: new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(70)]),
      country: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(15)]),
      city: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(15)]),
      region: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(15)]),
      postalCode: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      homePhone: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      extension: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      birthDate: new FormControl("", [Validators.required]),
      hireDate: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required, Validators.maxLength(60)]),
      titleOfCourtesy: new FormControl("", [Validators.required, Validators.maxLength(10)]),
      notes: new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(60)]),
      reportsTo: new FormControl("", [Validators.pattern(this.numberPattern)]),
      photo: '',
    });

  }

  editarEmployee():any {
    
    if (this.editEmpleados.valid) {
      this.service.editar(this.url,this.idEmployee ,this.editEmpleados.value).subscribe({
        complete: () => {
          Swal.fire(
            'Empledo Actulizado',
            '¡Los datos fueron fueron actulizados con exito!',
            'success'
          );
          this.router.navigateByUrl("employees");
        },
        error: (e) => {

          Swal.fire(
            'Error al Actualizar',
            '¡Ups! Ocurrio un error' ,
            'error'
          );
        }
        
      });
    }else{
      alert("Revisa el formulario");
    }

  }

  rellenarForm(num) {
    this.service.consultar(this.url, num).subscribe({
      next: (data) => {

        this.editEmpleados.setValue({
          firstName: data.data.firstName,
          lastName: data.data.lastName,
          address: data.data.address,
          country: data.data.country,
          city: data.data.city,
          region: data.data.region,
          postalCode: data.data.postalCode,
          homePhone: data.data.homePhone,
          extension: data.data.extension,
          birthDate: data.data.birthDate,
          hireDate: data.data.hireDate,
          title: data.data.title,
          titleOfCourtesy: data.data.titleOfCourtesy,
          notes: data.data.notes,
          reportsTo: data.data.reportsTo,
          photo: '',
        });
      }

    });
  }
  err = {

    "nombre": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 3."
      }
    ],
    "address": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      },
      {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 60."
      }
    ],
    "country": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 3."
      }, {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 20."
      }
    ],
    "postal": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se permiten números"
      },
    ],
    "homePhone": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se permiten números"
      },
    ],
    "fechas": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      }
    ],
    "notas": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      },
      {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 60."
      }
    ]
  };

}
