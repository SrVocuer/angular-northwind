import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-empleado',
  templateUrl: './registrar-empleado.component.html',
  styleUrls: ['./registrar-empleado.component.scss']
})
export class RegistrarEmpleadoComponent implements OnInit {
  empleados: FormGroup;
  url = "empleados/";
  select = [];
  numberPattern = '^[0-9]*$';


  constructor(
    public formulario: FormBuilder,
    private service: ApiService,
    private router: Router,
  ) {

    this.empleados = this.formulario.group({
      firstName: new FormControl("", [Validators.required, Validators.minLength(3)]),
      lastName: new FormControl("", [Validators.required, Validators.minLength(3)]),
      address: new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(70)]),
      country: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(15)]),
      city: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(15)]),
      region: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(15)]),
      postalCode: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      homePhone: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      extension: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      birthDate: new FormControl("", [Validators.required]),
      hireDate: new FormControl("", [Validators.required]),
      title: new FormControl("", [Validators.required, Validators.maxLength(60)]),
      titleOfCourtesy: new FormControl("", [Validators.required, Validators.maxLength(10)]),
      notes: new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(60)]),
      reportsTo: new FormControl("", [Validators.pattern(this.numberPattern)]),
      photo: '',
    });

  }

  ngOnInit(): void {
    this.consultar();

  }

  enviarEmployee(): any {

    if (this.empleados.valid) {
      this.service.registrar(this.url, this.empleados.value).subscribe({
        complete: () => {
          Swal.fire(
            'Empledo Registrado',
            '¡Nueva empleado registrado con exito!',
            'success'
          );
          this.router.navigateByUrl("employees");
        },
        error: (e) => {

          Swal.fire(
            'Error al Registrar',
            '¡Ups! Ocurrio un error',
            'error'
          );
        }

      });
    } else {
      alert("Revisa el formulario");
    }


  }

  consultar() {
    this.service.consultar(this.url, null).subscribe(
      {
        next: (s) => {
          return this.select = s.data;
        }
      }
    );
  }

  err = {

    "nombre": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 3."
      }
    ],
    "address": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      },
      {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 60."
      }
    ],
    "country": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 3."
      }, {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 20."
      }
    ],
    "postal": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se permiten números"
      },
    ],
    "homePhone": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se permiten números"
      },
    ],
    "fechas": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      }
    ],
    "notas": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimo de caráctares son 10."
      },
      {
        type: "maxlength",
        mensaje: "*El maximo de caráctares son 60."
      }
    ]
  };


}
