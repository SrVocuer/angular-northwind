import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  url = "empleados/";
  Employees = [];


  constructor(
    private service: ApiService,
    private router: Router,

  ) { }

  ngOnInit(): void {

    this.consultar();

  }

  consultar() {
    this.service.consultar(this.url, null).subscribe(response => {
      return this.Employees = response.data;
    });
  }
  
  borrarEmpleado(id) {
    Swal.fire({
      title: '¿Estás seguro de eliminar?',
      text: "Recuerda, no se puede revertir esto.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.service.eliminar(this.url, id).subscribe({
          complete: () => {
            Swal.fire(
              'Elminado',
              '¡Empleado ha sido eliminado!',
              'success'
            );

            // this.router.navigateByUrl('employees');
            this.consultar();
          },
          error: () => {
            Swal.fire(
              'Error al Eliminar',
              '¡No se pudo eliminar esta empleado, compruebe que no esté en uso!',
              'error'
            );
          }
        });
      }

    });
  }

}

