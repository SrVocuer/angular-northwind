import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { RegistrarProductsComponent } from './components/registrar-products/registrar-products.component';
import { EditarProductsComponent } from './components/editar-products/editar-products.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegistrarPedidoComponent } from './components/registrar-pedido/registrar-pedido.component';


@NgModule({
  declarations: [
    RegistrarProductsComponent,
    EditarProductsComponent,
    RegistrarPedidoComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ]
})
export class ProductsModule { }
