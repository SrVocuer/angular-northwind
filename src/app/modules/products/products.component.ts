import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  url = "productos/";
  productos = [];
  cat = [];


  constructor(
    private service: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.consultar();

  }

  consultar() {
    this.service.consultar(this.url, null).subscribe(request => {
      this.productos = request.data;
    });
  }

  eliminar(id) {
    Swal.fire({
      title: '¿Estás seguro de eliminar?',
      text: "Recuerda, no se puede revertir esto.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.service.eliminar(this.url, id).subscribe({
          complete: () => {
            Swal.fire(
              'Producto',
              '¡Se ha sido eliminado!',
              'success'
            );

            this.consultar();
          },
          error: () => {
            Swal.fire(
              'Error al Eliminar',
              '¡No se pudo eliminar, compruebe que no esté en uso!',
              'error'
            );
          }
        });
      }

    });
  }

}
