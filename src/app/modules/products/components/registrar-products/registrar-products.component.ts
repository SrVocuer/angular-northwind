import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-products',
  templateUrl: './registrar-products.component.html',
  styleUrls: ['./registrar-products.component.scss']
})
export class RegistrarProductsComponent implements OnInit {

  url = "productos/";
  productos = [];
  categorias = [];
  proveedores = [];
  formProductos: FormGroup;
  numberPattern = '^[0-9]*$';
  decimal = "[0-9]+(\.[0-9]+)?";
  constructor(
    private service: ApiService,
    private router: Router,
    public formulario: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.consultarCategoria();
    this.consultarProveedor();

    this.formProductos = this.formulario.group({
      productName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      unitPrice: new FormControl("", [Validators.required, Validators.pattern(this.decimal)]),
      unitsInStock: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      quantityPerUnit: new FormControl("", [Validators.required, Validators.minLength(4)]),
      categorias: new FormControl("", [Validators.required]),
      proveedores: new FormControl("", [Validators.required]),
      discontinued: new FormControl(0,),
    })

  }
  guardarProducto() {

    if (this.formProductos.valid) {
      this.service.registrar(this.url, this.formProductos.value).subscribe({
        complete: () => {
          Swal.fire(
            'Producto Registrado',
            '¡Nueva producto registrado con exito!',
            'success'
          );
          this.router.navigateByUrl("products");
        },
        error: (e) => {

          Swal.fire(
            'Error al Registrar',
            '¡Ups! Ocurrio un error',
            'error'
          );
        }

      });
    } else {
      alert("Revisa el formulario");
    }


  }

  consultarCategoria() {
    this.service.consultar("categorias/", null).subscribe(d => {
      this.categorias = d.data;
    });
  }
  consultarProveedor() {
    this.service.consultar("proveedores/", null).subscribe(d => {
      this.proveedores = d.data;
    });
  }

  err = {
    "name": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 4."
      }
    ],
    "units": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se admiten campos numericos"
      }
    ],
    "fk": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      }
    ]
  }
}
