import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-pedido',
  templateUrl: './registrar-pedido.component.html',
  styleUrls: ['./registrar-pedido.component.scss']
})
export class RegistrarPedidoComponent implements OnInit {


  url = "productos/";
  id;
  datosProductos;
  productos: FormGroup;
  numberPattern = '^[0-9]*$';


  orden = {};


  constructor(
    private service: ApiService,
    private router: Router,
    public formulario: FormBuilder,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.id = this.activeRoute.snapshot.paramMap.get('id');
    this.productos = this.formulario.group({
      productName: '',
      unitPrice: '',
      unitsInStock: '',
      quantityPerUnit: '',
      reorderLevel: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      proveedores: '',
    });

    this.llenarForm(this.id);
  }

  enviarPedido() {

    this.datosProductos['reorderLevel'] = this.productos.get('reorderLevel').value;


    this.orden = {
      "productId": this.id,
      "unitPrice": this.productos.get('unitPrice').value,
      "quantity": this.productos.get('reorderLevel').value,
      "discount": 0.3,
    }

    // console.log(this.orden);

    this.service.editar(this.url, this.id, this.datosProductos).subscribe({
      complete: () =>{
        
        this.service.registrar("pedido", this.orden).subscribe();

        Swal.fire(
          'Producto Actualizado',
          '¡Producto actualizado con exito!',
          'success'
        );
        this.router.navigateByUrl("products");

      }
    });


  }

  llenarForm(id) {
    this.service.consultar("productos/", id).subscribe(d => {
      this.datosProductos = d.data;

      this.productos.setValue({
        productName: d.data.productName,
        quantityPerUnit: d.data.quantityPerUnit,
        unitPrice: d.data.unitPrice,
        unitsInStock: d.data.unitsInStock,
        reorderLevel: d.data.reorderLevel,
        proveedores: d.data.nombreProve,
      });

    });

  }


}
