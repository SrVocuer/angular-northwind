import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-products',
  templateUrl: './editar-products.component.html',
  styleUrls: ['./editar-products.component.scss']
})
export class EditarProductsComponent implements OnInit {

  url = "productos/";
  id;
  categorias = [];
  proveedores = [];
  formEditarProductos: FormGroup;
  numberPattern = '^[0-9]*$';
  decimal = "[0-9]+(\.[0-9]+)?";

  constructor(
    private service: ApiService,
    private router: Router,
    public formulario: FormBuilder,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {


    this.id = this.activeRoute.snapshot.paramMap.get('id');

    this.llenarForm(this.id);

    this.formEditarProductos = this.formulario.group({
      productName: new FormControl("", [Validators.required, Validators.minLength(4)]),
      unitPrice: new FormControl("", [Validators.required, Validators.pattern(this.decimal)]),
      unitsInStock: new FormControl("", [Validators.required, Validators.pattern(this.numberPattern)]),
      quantityPerUnit: new FormControl("", [Validators.required, Validators.minLength(4)]),
      categorias: new FormControl("", [Validators.required]),
      proveedores: new FormControl("", [Validators.required]),
      discontinued: '',
    });
    this.consultarCategoria();
    this.consultarProveedor();
  }

  editarProducto() {
    // console.log(this.formEditarProductos.value);
    
    if (this.formEditarProductos.valid) {

      this.service.editar(this.url, this.id, this.formEditarProductos.value).subscribe({
        complete: () => {
          Swal.fire(
            'Producto Actualizado',
            '¡Producto actualizado con exito!',
            'success'
          );
          this.router.navigateByUrl("products");
        },
        error: () => {

          Swal.fire(
            'Error al Registrar',
            '¡Ups! Ocurrio un error',
            'error'
          );
        }

      });
    } else {
      alert("Revisa el formulario");
    }

  }

  consultarCategoria() {
    this.service.consultar("categorias/", null).subscribe(d => {
      this.categorias = d.data;
    });
  }
  consultarProveedor() {
    this.service.consultar("proveedores/", null).subscribe(d => {
      this.proveedores = d.data;
    });
  }

  llenarForm(id) {
    this.service.consultar(this.url, id).subscribe(res => {
      
      this.formEditarProductos.setValue({
        productName: res.data.productName,
        unitPrice: res.data.unitPrice,
        unitsInStock: res.data.unitsInStock,
        quantityPerUnit: res.data.quantityPerUnit,
        categorias: res.data.categorias,
        proveedores: res.data.proveedores,
        discontinued: res.data.discontinued,
      });
    });
  }



  err = {
    "name": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "minlength",
        mensaje: "*El minimoo de caráctares son 4."
      }
    ],
    "units": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      },
      {
        type: "pattern",
        mensaje: "*Solo se admiten campos numericos"
      }
    ],
    "fk": [
      {
        type: "required",
        mensaje: "*Campo obligatorio."
      }
    ]
  }
}
