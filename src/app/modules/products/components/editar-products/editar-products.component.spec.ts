import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProductsComponent } from './editar-products.component';

describe('EditarProductsComponent', () => {
  let component: EditarProductsComponent;
  let fixture: ComponentFixture<EditarProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarProductsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditarProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
