import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarProductsComponent } from './components/editar-products/editar-products.component';
import { RegistrarProductsComponent } from './components/registrar-products/registrar-products.component';
import { ProductsComponent } from './products.component';

import { RegistrarSuppliersComponent } from '../suppliers/components/registrar-suppliers/registrar-suppliers.component';
import { RegistrarPedidoComponent } from './components/registrar-pedido/registrar-pedido.component';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: ProductsComponent,
    title: 'Products',
  },
  {
    path: 'registrar',
    component: RegistrarProductsComponent,
    data: { title: 'New' },
  },
  {
    path: 'editar/:id',
    component: EditarProductsComponent,
    data: { title: 'Edit' },
  },
  {
    path: 'pedido/:id',
    component: RegistrarPedidoComponent,
    data: { title: 'New Order' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(APP_ROUTES)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
