import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class MenuService {
  public apiRoot: string;

  private currentMenuSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  public readonly currentMenu: Observable<any> = this.currentMenuSubject.asObservable();

  constructor(private http: HttpClient, private configService: ConfigService) {
    this.apiRoot = this.configService.getConfig().apiMenu;
  }

  public get(): Array<any> {
    return this.currentMenuSubject.getValue()
  }

  public fetchMenu(queryParams?: any): void {
    this.http.get(this.apiRoot, { params: queryParams }).subscribe({
      next: (response: any) => this.currentMenuSubject.next(response.data.menu)
    });
  }

}
