import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class NotificationsService {
  public apiRoot: string;

  private currentNotificationsSubject: Subject<Array<any>> = new Subject();
  public readonly currentNotifications: Observable<any> = this.currentNotificationsSubject.asObservable();
  private lastValue: Array<any> | undefined;

  constructor(public http: HttpClient, private configService: ConfigService) {
    this.apiRoot = this.configService.getConfig().webApiBaseUrl;
  }

  public fetchNotifications(queryParams?: any): void {
    this.http.get(`${this.apiRoot}/notifications`, { params: queryParams }).subscribe({
      next: (response: any) => {
        this.currentNotificationsSubject.next(response)
      }
    });
  }

  public hasNotifications() {
    return this.lastValue
  }

}
