import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  API: string = "http://localhost:8080/api/v1/";

  constructor(private clienteHttp: HttpClient) { }

  consultar(url: string, id: number) {

    if (id != null)
      return this.clienteHttp.get<any>(this.API + url + id);
    else
      return this.clienteHttp.get<any>(this.API + url);

  }

  registrar(url: string, datos: any,): Observable<any> {

    return this.clienteHttp.post(this.API + url, datos);
  }

  editar(url: string, id: number, datos: any) {

    return this.clienteHttp.put(this.API + url + id, datos);
  }

  eliminar(url: string, id: number) {
    return this.clienteHttp.delete(this.API + url + id);
  }
}
