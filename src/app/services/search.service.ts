import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class SearchService {
  public apiRoot: string;

  private currentSearchSubject: Subject<Array<any>> = new Subject();
  public readonly currentSearch: Observable<any> = this.currentSearchSubject.asObservable();
  private lastValue: Array<any> | undefined;

  constructor(public http: HttpClient, private configService: ConfigService) {
    this.apiRoot = this.configService.getConfig().webApiBaseUrl;
  }

  public fetchSearch(queryParams?: any): void {
    this.http.get(`${this.apiRoot}/search`, { params: queryParams }).subscribe({
      next: (response: any) => {
        this.currentSearchSubject.next(response.data)
      }
    });
  }

  public hasSearch() {
    return this.lastValue
  }

}
