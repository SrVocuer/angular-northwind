// https://stackoverflow.com/questions/51812991/simple-javascript-encryption-using-libsodium-js-in-this-sandbox-demo

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as _sodium from 'libsodium-wrappers'
import concatTypedArray from 'concat-typed-array';

@Injectable({
  providedIn: 'root'
})
export class SecureStorageService {

  public loaded;
  public key!: Uint8Array;
  private nonceBytes!: number;

  constructor() {
    this.loaded = _sodium.ready;
  }

  // INITIALIZATION

  public load(): void {
    this.loaded.then(() => {
      this.nonceBytes = _sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES;
      this.key = _sodium.crypto_generichash(32, _sodium.from_string(environment.appId));
    })
  }

  // UTILS FUNCTIONS

  private u_atob(ascii: string) {        //https://stackoverflow.com/a/43271130/
    return Uint8Array.from(window.atob(ascii), c => c.charCodeAt(0));
  }
  private u_btoa(buffer: ArrayBufferLike) {       //https://stackoverflow.com/a/43271130/
    let binary = [];
    let bytes = new Uint8Array(buffer);
    for (let i = 0, il = bytes.byteLength; i < il; i++) {
      binary.push(String.fromCharCode(bytes[i]));
    }
    return window.btoa(binary.join(""));
  }

  // LIBSODIUM FUNCTIONS

  /**
   * @param {string} message
   * @param {string} key
   * @returns {Uint8Array}
   */
  private encrypt_and_prepend_nonce(message: string) {
    let nonce = _sodium.randombytes_buf(this.nonceBytes);
    const encrypted = _sodium.crypto_aead_xchacha20poly1305_ietf_encrypt(message, null, nonce, nonce, this.key);
    const nonce_and_ciphertext = concatTypedArray(Uint8Array, nonce, encrypted); //https://github.com/jedisct1/libsodium.js/issues/130#issuecomment-361399594
    return nonce_and_ciphertext;
  }

  /**
   * @param {Uint8Array} nonce_and_ciphertext
   * @param {string} key
   * @returns {string}
   */
  private decrypt_after_extracting_nonce(nonce_and_ciphertext: Uint8Array) {
    (async () => {
      await _sodium.ready;
    })();
    let nonce = nonce_and_ciphertext.slice(0, this.nonceBytes); //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/slice      
    let ciphertext = nonce_and_ciphertext.slice(this.nonceBytes);
    return _sodium.crypto_aead_xchacha20poly1305_ietf_decrypt(nonce, ciphertext, null, nonce, this.key, "text");
  }

  /**
   * @param {string} message
   * @param {string} key
   * @returns {string}
   */
  private encrypt(message: string) {
    const uint8ArrayMsg = this.encrypt_and_prepend_nonce(message);
    return this.u_btoa(uint8ArrayMsg); //returns ascii string of garbled text
  }

  /**
   * @param {string} nonce_and_ciphertext_str
   * @param {string} key
   * @returns {string}
   */
  private decrypt(nonce_and_ciphertext_str: string) {
    try { 
      const nonce_and_ciphertext = this.u_atob(nonce_and_ciphertext_str); //converts ascii string of garbled text into binary
      return this.decrypt_after_extracting_nonce(nonce_and_ciphertext)
    } catch (error) {
      localStorage.clear()
      sessionStorage.clear()
      return ''
    }
  }

  // PUBLIC FUNCTIONS

  public setItem(key: string, value: string): void {
    const secureValue = this.encrypt(value);
    localStorage.setItem(key, secureValue);
  }

  public getItem(key: string): string {
    const secureValue = localStorage.getItem(key);
    if (!secureValue || secureValue.length === 0) {
      return ''
    }
    return this.decrypt(secureValue);
  }

}