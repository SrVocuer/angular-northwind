import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_URL = environment.authIdp.CST.apiUrl;

@Injectable()
export class CustomService {

  constructor(private http: HttpClient) { }

  private _handleError(error: HttpErrorResponse) {
    let errorMessage = 'No se obtuvo respuesta del servidor de autenticación';
    if (error.error.data) errorMessage = error.error.data.userMessage;
    return throwError(() => new Error(errorMessage))
  }

  public login(credentials: any): Observable<string> {
    return this.http
      .post(API_URL + '/auth/login', credentials)
      .pipe(
        map(({ data }: any) => data.access_token),
        catchError(this._handleError)
      )
  }

}