
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_URL = environment.authIdp.SSO.apiUrl;

@Injectable()
export class SsoService {

  private recaptchaParams = {
    channel: environment.authIdp.SSO.channel,
    origin: 'web'
  }

  constructor(
    private http: HttpClient
  ) { }

  private _handleError(error: HttpErrorResponse) {
    let errorMessage = 'No se obtuvo respuesta del servidor de autenticación. Verifica tu conexión a red Interna.';
    if (error.error.meta) errorMessage = error.error.meta.error.userMessage;
    else if (error.error.data?.userMessage) errorMessage = error.error.data.userMessage;
    return throwError(() => new Error(errorMessage))
  }

  // ------ Bloque de métodos de lógica de autenticación

  public loginHuella(credentials: any): Observable<string> {
    return this.http
      .post(API_URL + '/v2/login', credentials)
      .pipe(
        map(({ data }: any) => data.token ),
        catchError(this._handleError)
      )
  }

  public loginColaboradorDigital(credentials: any): Observable<string> {
    credentials.loginType = 10
    return this.http
      .post(API_URL + '/v2/login', { ...this.recaptchaParams, ...credentials })
      .pipe(
        map(({ data }: any) => data.token),
        catchError(this._handleError)
      )
  }

  public loginAuthCode(authCode: any): Observable<string> {
    return this.http
      .post<any>(API_URL + '/v1/login/code', { authCode, appId: environment.authIdp.SSO.appId })
      .pipe(
        map(({ data }: any) => data.token),
        catchError(this._handleError)
      )
  }

}
