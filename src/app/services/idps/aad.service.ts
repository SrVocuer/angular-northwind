import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MsalBroadcastService, MsalGuardConfiguration, MsalService, MSAL_GUARD_CONFIG } from '@azure/msal-angular';
import { EventMessage, EventType, InteractionStatus, RedirectRequest } from '@azure/msal-browser';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SecureStorageService } from '../secure-storage.service';

const GRAPH_URL = environment.authIdp.AAD.graphUrl;
const GRAPH_PARAMS = 'displayName,givenName,surname,employeeId,mail,jobTitle,department,companyName';
const GRAPH_ENDPOINT = `${GRAPH_URL}/v1.0/me?$select=${GRAPH_PARAMS}`;

@Injectable()
export class AadService {

  private readonly _destroying$ = new Subject<void>();

  constructor(
    @Inject(MSAL_GUARD_CONFIG) private msalGuardConfig: MsalGuardConfiguration,
    private broadcastService: MsalBroadcastService,
    private authService: MsalService,
    private http: HttpClient,
    private storageService: SecureStorageService,
  ) {
    const idpSetted = typeof localStorage.getItem('idp') == "string";
    const aadEnabled = environment.authMechanisms.azureAD;
    if (aadEnabled && idpSetted) {
      this.listenInteraction()
      this.listenAuth()
    }
  }

  private listenInteraction() {
    this.broadcastService.inProgress$
      .pipe(
        filter((status: InteractionStatus) => status === InteractionStatus.None),
        takeUntil(this._destroying$)
      ).subscribe ({
        next: () => {
          this.checkAndSetActiveAccount()
        }
      })
  }
  private listenAuth() {
    this.broadcastService.msalSubject$
      .pipe(
        filter((msg: EventMessage) => msg.eventType === EventType.LOGIN_SUCCESS),
        takeUntil(this._destroying$)
      ).subscribe({
        next: (authentication: any) => {
          if (authentication.payload['accessToken']) {
            const token = `${authentication.payload['tokenType']} ${authentication.payload['accessToken']}`
            this.storageService.setItem('token', token)
          }
        }
      })
  }

  private checkAndSetActiveAccount() {
    let activeAccount = this.authService.instance.getActiveAccount();
    if (!activeAccount && this.authService.instance.getAllAccounts().length > 0) {
      let accounts = this.authService.instance.getAllAccounts();
      this.authService.instance.setActiveAccount(accounts[0]);
    }
  }
  public fetchProfile(): Observable<any> {
    return this.http.get(GRAPH_ENDPOINT)
  }

  public login() {
    if (this.msalGuardConfig.authRequest) {
      this.authService.loginRedirect({ ...this.msalGuardConfig.authRequest } as RedirectRequest);
    } else {
      this.authService.loginRedirect();
    }
  }

}
