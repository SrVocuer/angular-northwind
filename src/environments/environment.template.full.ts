// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  appId: '0a78511d-7609-43f0-ae41-e9508c8f50e6', // Código individual la de la aplicación
  production: false,
  configFile: 'assets/settings/settings.json',
  // Ubicación de archivo de variables de aplicación
  // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.6bu44umfcr2b
  recaptcha: {
    // Es necesario sólo para colaboradorDigital
    scriptUrl: 'https://www.google.com/recaptcha/enterprise.js',
    siteKey: '6LdHZ_0dAAAAAE_OxObOIF3odKLJKXj2Vu_GZBHk' // fijo para ambiente de desarrollo
  },
  authMechanisms: {
    // Variedad de mecanismos de autenticación disponibles
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.uh893752y2p4
    azureAD: true,              // AAD https://sites.google.com/coppel.com/developers/servicios/azure-ad?authuser=0
    huella: true,               // SSO https://sites.google.com/coppel.com/developers/frameworks/webclient/componentes/componente-huella?authuser=0
    colaboradorDigital: true,   // SSO https://sites.google.com/coppel.com/developers/frameworks/webclient/novedades/sso-estatus?authuser=0
    authCode: true,             // SSO
    custom: false               // CST
  },
  authIdp: {
    // Variables sensibles para cada tipo de autenticación
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.oerg1eog2jl7
    AAD: {
      tenantId: '76d81621-b9a9-4786-bb8f-a2efa839eee2', // Organización en Azure Active Directory
      clientId: '11a7a763-48d7-4e32-819e-55fd5b7be8c1', // Código de aplicación único dentro del tenant al que pertenece
      authorityUrl: 'https://login.microsoftonline.com',
      redirectUri: 'http://localhost:4200',
      graphUrl: 'https://graph.microsoft.com',
    },
    SSO: {
      appId: '0a78511d-7609-43f0-ae41-e9508c8f50e6',
      channel: 'ecommerce-dev-244022', // Corresponde al campo "Proyecto" que se obtiene al solicitar un DNS (Es necesario sólo para colaboradorDigital)
      apiUrl: 'https://authentication-dev.coppel.io:8443/api/sso'
      // Deberá coincidir con la ruta en fake api
      // Para el caso de fake API la ubicación es fake-api/authentication/sso.js
    },
    CST: {
      apiUrl: 'http://localhost:3200'
    }
  },
  interceptorApproach: [
    // APIs y fuentes de datos que requieran recibir el token y headers
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.qhvjo6nftyz9
    'http://localhost:3200'
  ],
  idpSpecificationRequired: true
  // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.qhvjo6nftyz9
};

// Se dieron de alta los DNS para el tema del reCAPTCHA.
// Proyecto: ecommerce-dev-244022
// DNS: webclient-dev-230451.com

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
