
// Este archivo es una copia de environment.template.full.ts
// Elimina este archivo y copia un template acorde a tus necesidades y luego cambia el nombre de la copia a environment.ts
// Lee la documentación dentro del archivo de template seleccionado en los links
// Sustituye con tus secrets según el template seleccionado

export const environment = {
  appId: '0a78511d-7609-43f0-ae41-e9508c8f50e6',
  production: false,
  configFile: 'assets/settings/settings.json',
  recaptcha: {
    scriptUrl: 'https://www.google.com/recaptcha/enterprise.js',
    siteKey: '6LdHZ_0dAAAAAE_OxObOIF3odKLJKXj2Vu_GZBHk'
  },
  authMechanisms: {
    azureAD: true,
    huella: true,
    colaboradorDigital: true,
    authCode: true,
    custom: true
  },
  authIdp: {
    AAD: {
      tenantId: '76d81621-b9a9-4786-bb8f-a2efa839eee2',
      clientId: '11a7a763-48d7-4e32-819e-55fd5b7be8c1',
      authorityUrl: 'https://login.microsoftonline.com',
      redirectUri: 'http://localhost:4200',
      graphUrl: 'https://graph.microsoft.com',
    },
    SSO: {
      appId: '0a78511d-7609-43f0-ae41-e9508c8f50e6',
      channel: 'ecommerce-dev-244022',
      apiUrl: 'https://authentication-dev.coppel.io:8443/api/sso'
    },
    CST: {
      apiUrl: 'http://localhost:3200'
    }
  },
  interceptorApproach: [
    'http://localhost:3200'
  ],
  idpSpecificationRequired: true
};
