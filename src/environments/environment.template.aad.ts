// Archivo de configuración template para IDP Azure Active Directory
export const environment = {
  appId: '11a7a763-48d7-4e32-819e-55fd5b7be8c1', // Código individual la de la aplicación
  production: false,
  configFile: 'assets/settings/settings.json',
  // Ubicación de archivo de variables de aplicación
  // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.6bu44umfcr2b
  recaptcha: {
    scriptUrl: '',
    siteKey: ''
  },
  authMechanisms: {
    // Variedad de mecanismos de autenticación disponibles
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.uh893752y2p4
    azureAD: true, // AAD https://sites.google.com/coppel.com/developers/servicios/azure-ad?authuser=0
    huella: false,
    colaboradorDigital: false,
    authCode: false,
    custom: false
  },
  authIdp: {
    // Variables sensibles para cada tipo de autenticación
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.oerg1eog2jl7
    AAD: {
      tenantId: '76d81621-b9a9-4786-bb8f-a2efa839eee2', // Organización en Azure Active Directory
      clientId: '11a7a763-48d7-4e32-819e-55fd5b7be8c1', // Código de aplicación único dentro del tenant al que pertenece
      authorityUrl: 'https://login.microsoftonline.com',
      redirectUri: 'http://localhost:4200',
      graphUrl: 'https://graph.microsoft.com',
    },
    SSO: {
      appId: '',
      channel: '',
      apiUrl: ''
    },
    CST: {
      apiUrl: 'http://localhost:3200'
    }
  },
  interceptorApproach: [
    // APIs y fuentes de datos que requieran recibir el token y headers
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.qhvjo6nftyz9
    'http://localhost:3200'
  ],
  idpSpecificationRequired: true
  // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.qhvjo6nftyz9
};