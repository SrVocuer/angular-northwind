export const environment = {
  appId: '',
  production: true,
  configFile: 'assets/settings/settings.prod.json', // fijo para ambiente productivo
  recaptcha: {
    scriptUrl: 'https://www.google.com/recaptcha/enterprise.js',
    siteKey: '' // 6Ld6IoEeAAAAAGi1nKkrTKwcKSRcd8NyfPGtBzpY (fijo para ambiente productivo)
  },
  authMechanisms: {
    azureAD: true,
    huella: true,
    colaboradorDigital: true,
    authCode: true,
    custom: false
  },
  authIdp: {
    AAD: {
      tenantId: '',
      clientId: '',
      authorityUrl: 'https://login.microsoftonline.com',
      redirectUri: 'http://localhost:4200',
      graphUrl: 'https://graph.microsoft.com',
    },
    SSO: {
      appId: '',
      channel: '',
      apiUrl: 'https://authentication.coppel.io/api/sso'
    },
    CST: {
      apiUrl: 'http://localhost:3200'
    }
  },
  interceptorApproach: [
    'http://localhost:3200'
  ],
  idpSpecificationRequired: false
};