export const environment = {
  appId: '',
  production: false,
  configFile: 'assets/settings/settings.json',
  recaptcha: {
    scriptUrl: '',
    siteKey: ''
  },
  authMechanisms: {
    azureAD: false,
    huella: false,
    colaboradorDigital: false,
    authCode: false,
    custom: false
  },
  authIdp: {
    AAD: {
      tenantId: '',
      clientId: '',
      authorityUrl: '',
      redirectUri: '',
      graphUrl: '',
    },
    SSO: {
      appId: '',
      channel: '',
      apiUrl: ''
    },
    CST: {
      apiUrl: ''
    }
  },
  interceptorApproach: [
    'http://localhost:3200'
  ],
  idpSpecificationRequired: true
};