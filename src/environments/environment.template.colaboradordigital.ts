// Archivo de configuración template para IDP SSO Colaborador Digital

export const environment = {
  appId: '0a78511d-7609-43f0-ae41-e9508c8f50e6', // Código individual la de la aplicación
  production: false,
  configFile: 'assets/settings/settings.json',
  // Ubicación de archivo de variables de aplicación
  // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.6bu44umfcr2b
  recaptcha: {
    // Es necesario sólo para colaboradorDigital
    scriptUrl: 'https://www.google.com/recaptcha/enterprise.js', // fijo
    siteKey: '6LdHZ_0dAAAAAE_OxObOIF3odKLJKXj2Vu_GZBHk' // fijo para ambiente de desarrollo
  },
  authMechanisms: {
    // Variedad de mecanismos de autenticación disponibles
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.uh893752y2p4
    azureAD: false,
    huella: false,
    colaboradorDigital: true, // SSO https://sites.google.com/coppel.com/developers/frameworks/webclient/novedades/sso-estatus?authuser=0
    authCode: false,
    custom: false
  },
  authIdp: {
    // Variables sensibles para cada tipo de autenticación
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.oerg1eog2jl7
    AAD: {
      tenantId: '',
      clientId: '',
      authorityUrl: '',
      redirectUri: '',
      graphUrl: '',
    },
    SSO: {
      appId: '0a78511d-7609-43f0-ae41-e9508c8f50e6',
      channel: 'ecommerce-dev-244022', // Corresponde al campo "Proyecto" que se obtiene al solicitar un DNS (Es necesario sólo para colaboradorDigital)
      apiUrl: 'https://authentication-dev.coppel.io:8443/api/sso'
      // Deberá coincidir con la ruta en fake api
      // Para el caso de fake API la ubicación es fake-api/authentication/sso.js
    },
    CST: {
      apiUrl: 'http://localhost:3200'
    }
  },
  interceptorApproach: [
    // APIs y fuentes de datos que requieran recibir el token y headers
    // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.qhvjo6nftyz9
    'http://localhost:3200'
  ],
  idpSpecificationRequired: true
  // https://sites.google.com/coppel.com/developers/frameworks/webclient/documentacion?authuser=0#h.qhvjo6nftyz9
};

// Iniciar proyecto con npm run dns

// Se dieron de alta los DNS para reCAPTCHA.
// Proyecto: ecommerce-dev-244022
// DNS: webclient-dev-230451.com