
const jwt = require('jsonwebtoken');
const { handleError } = require('../utils')

const SECRET_KEY = '123456789';
const expiresIn = '8h';

// Create custom token from a payload
function createToken(payload) {
  return jwt.sign(payload, SECRET_KEY, { expiresIn });
}

// Verify custom token
function verifyToken(token) {
  const verifyTokenResult = jwt.verify(token, SECRET_KEY, (err, decode) => (decode !== undefined ? decode : err));
  if (verifyTokenResult instanceof Error) throw handleError(401, 'Unauthorized', verifyTokenResult);
  return verifyTokenResult
}

// Me custom token
function meToken(token) {
  return jwt.decode(token);
}

module.exports = {
  createToken,
  verifyToken,
  meToken
}