const axios = require('axios');
const { handleError } = require('../utils')

const ssoApi = 'https://authentication-dev.coppel.io:8443/api/sso';
const headers = (token) => ({ headers: { 'Authorization': token }})

// Parse SSO error
function ssoError(error) {
    if (!error.hasOwnProperty('response')) {
        return handleError(
            404,
            'No se obtuvo respuesta de servidor',
            `Error connection with ${ssoApi}`
        )
    }
    return handleError(
        error.response.status,
        error.response.statusText,
        error.response.data.data
    )
}

// Verify SSO token
async function verifyToken(token) {
    try {
        const verifyReq = await axios.post(`${ssoApi}/v1/verify`, {}, headers(token));
        return verifyReq.data.data;
    } catch (error) {
        throw ssoError(error);
    }
}

// Me SSO token
async function meToken(token) {
    try {
        const verifyReq = await axios.get(`${ssoApi}/v2/me`, headers(token));
        return verifyReq.data.data;
    } catch (error) {
        throw ssoError(error);
    }
}

module.exports = { verifyToken, meToken };