const axios = require('axios');
const { handleError } = require('./utils')

const recaptchaApi = 'https://www.google.com/recaptcha/api';
const recaptchaSecret = '6Le1ct4iAAAAAKJm862L4wpE-aglzAQVH0LWQ4Ub';

// Verify recaptcha token
async function verifyToken(token) {
    try {
        const verifyReq = await axios.post(
            `${recaptchaApi}/siteverify`,
            `secret=${recaptchaSecret}&response=${token}`,
            { "Content-Type": "application/x-www-form-urlencoded" }
        );
        return verifyReq.data;
    } catch (error) {
        console.log(error);
        throw handleError(
            error.status,
            'Error en validación de reCAPTCHA',
            error.data
        )
    }
}

module.exports = { verifyToken };