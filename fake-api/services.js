const fs = require('fs');
const localdb = JSON.parse(fs.readFileSync('./db.json', 'UTF-8'));

function findUserByEmployeeId(employeeId) {
    return localdb.users.find(({ numeroempleado }) => numeroempleado == employeeId);
};

function searchInRandomData(search) {
    return localdb.searchs.filter(randomData => {
        const mergedValues = Object.values(randomData).join(' ').toLowerCase();
        return mergedValues.includes(search);
    })
};

module.exports = { findUserByEmployeeId, searchInRandomData };