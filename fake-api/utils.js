
const handleError = (errorCode, userMessage, devMessage) => ({
    meta: { status: 'ERROR', count: 1 },
    data: { errorCode, userMessage, devMessage }
});

module.exports = { handleError };