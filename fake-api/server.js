
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

const { handleError } = require('./utils')
const services = require('./services');
const sso = require('./authentication/sso');
const cst = require('./authentication/cst');

server.use(middlewares);
server.use(jsonServer.bodyParser);

// ----------------------- INTERCEPTOR

server.use(/^(?!\/auth).*$/, async (req, res, next) => {
  const token = req.headers.authorization;
  const method = req.headers['idp'];
  // Verificación de headers
  if (!token) {
    res.status(401).json(handleError(401, 'Headers incompletos', 'No se obtuvo el token'));
    return;
  }
  if (!method) {
    res.status(401).json(handleError(401, 'Headers incompletos', 'No se obtuvo el método de autenticación'));
    return;
  }
  // Verificación de token segun idP
  try {
    switch (method) {
      case 'AAD':
        next();
        break;
      case 'SSO':
        await sso.verifyToken(token);
        next();
        break;
      case 'CST':
        cst.verifyToken(token)
        next();
        break;
      default:
        res.status(422).json(
          handleError(422, 'Headers corruptos', 'No existe idP para validar el token')
        );
    }
  } catch (error) {
    if (error.data.errorCode) {
      res.status(error.data.errorCode).json(error);
    } else {
      res.status(404).json(error);
    }
  }

});

// ----------------------- ME

server.get('/me', async (req, res) => {
  const token = req.headers.authorization;
  const method = req.headers['idp'];
  let idpuser = undefined;
  let dbuser = undefined;
  // Verificación de usuario segun idP
  try {
    switch (method) {
      case 'AAD':
        break;
      case 'SSO':
        idpuser = await sso.meToken(token);
        dbuser = services.findUserByEmployeeId(idpuser.user)
        break;
      case 'CST':
        idpuser = cst.meToken(token);
        dbuser = services.findUserByEmployeeId(idpuser.username)
        break;
    }
  } catch (error) {
    if (error.data.errorCode) {
      res.status(error.data.errorCode).json(error);
    } else {
      res.status(404).json(error);
    }
    return;
  }
  // Verificación usuario en BD local
  if (!dbuser) {
    res.status(401).json(
      handleError(404, 'No se puede obtener los datos', 'Usuario no existente en la base de datos')
    );
    return;
  }
  // Si el usuario existe en los registros de idp y en la bd local los devuelve
  res.status(200).json({ meta: {}, data: {...dbuser, ...idpuser, idP: method} });
});

// ----------------------- CST LOGIN

server.post('/auth/login', (req, res) => {
  const { username } = req.body;
  const dbuser = services.findUserByEmployeeId(username)
  if (!dbuser) {
    res.status(401).json(
      handleError(401, 'Usuario Invalido', `No se encontró ${username} en db.json`)
    );
    return;
  }
  // En un servidor real aquí habría lógica extra para validar la contraseña
  const access_token = cst.createToken({ username });
  res.status(200).json({ meta: {}, data: { dbuser, access_token } });
});

// ----------------------- Métodos para Angular

server.get('/centros/pdf', (req, res) => {
  fs.readFile('src/assets/pdf/sample.pdf', (error, data) => {
    if (error) {
      res.json({ status: 'error', msg: error });
    } else {
      res.writeHead(200, { 'Content-Type': 'application/pdf' });
      res.write(data);
      res.end();
    }
  });
});
server.get('/search', (req, res) => {
  const { query } = req;
  const search = query.search;
  let data = []
  if (search) {
    data = services.searchInRandomData(search);
  }
  res.status(200).json({ meta: {}, data: data });
});

// ----------------------- INICIALIZACIÓN DE SERVER

server.use(router);
const args = require('minimist')(process.argv.slice(2));
const port = !args['port'] ? 3200 : args['port'];
const host = !args['host'] ? '0.0.0.0' : args['host'];

server.listen(port, host, () => {
  console.log(`JSON Server is running ${host} on port ${port}`);
});
